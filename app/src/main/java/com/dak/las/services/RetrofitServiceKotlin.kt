import android.util.Log
import com.dak.las.interfaces.ApiServiceKotlin
import com.dak.las.utilities.Constant
import com.dak.las.utilities.SupportInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitServiceKotlin{

    companion object {
        fun getRetrofit(): Retrofit {
            val okHttpClient = OkHttpClient.Builder().apply {
                readTimeout(20, TimeUnit.SECONDS)
                connectTimeout(20, TimeUnit.SECONDS)
                addInterceptor(SupportInterceptor())
            }

            val client = okHttpClient.build()
            val retrofit = Retrofit.Builder()
                    .baseUrl(Constant.SERVICE_BASE)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
            return retrofit
        }
    }
}
