package com.dak.las.interfaces

import com.dak.las.models.ValueDynamicResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface  ApiServiceKotlin{


    @Multipart
    @POST("get_all_survey")
    fun getAllSurvey(@Part("body") body:String, @Part("images") images : String) : Call<String>

    @Multipart
    @POST("get_detail_survey")
    fun getDetailSurvey(@Part("body") body:String, @Part("images") images : String) : Call<ValueDynamicResult>

}