package com.dak.las.interfaces;


import com.dak.las.models.Form.Form;
import com.dak.las.models.Survey.Survey;
import com.dak.las.models.Survey.TakingSurvey;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface ApiService {

    @Multipart
    @POST("check_msisdn")
    Call<String> check_msisdn(@Part("body") String body,@Part("images") String images);

    @Multipart
    @POST("login")
    Call<String> login(@Part("body") String body,@Part("images") String images);

    @Multipart
    @POST("get_all_survey")
    Call<Survey.ValueSurvey> getAllSurvey(@Part("body") String body,@Part("images") String images);

    @Multipart
    @POST("get_survey")
    Call<Survey.ValueSurvey> getAllMySurvey(@Part("body") String body,@Part("images") String images);

    @Multipart
    @POST("create_transaction")
    Call<String> sendForm(
            @Part("body") String json,
            @Part ("images")String image);

    @Multipart
    @POST("take_survey")
    Call<String> takeSurvey(
            @Part("body") String json,
            @Part ("images")String image);

    @Multipart
    @POST("initialize")
    Call<String> initialize(
            @Part("body") String json,
            @Part ("images")String image);

    @Multipart
    @POST("cancel_survey")
    Call<String> cancelSurvey(
            @Part("body") String json,
            @Part ("images")String image);


    @Multipart
    @POST("get_survey")
    Call<Survey.ValueSurvey> getOnGoingSurvey(
            @Part("body") String json,
            @Part ("images")String image);

    @Multipart
    @POST("get_transaction")
    Call<String> getFilledData(
            @Part("body") String json,
            @Part ("images")String image);

    @Multipart
    @POST("update_transaction")
    Call<String> updateSurvey(
            @Part("body") String json,
            @Part ("images")String image);

    @Multipart
    @POST("search")
    Call<Survey.ValueSurvey> search(
            @Part("body") String json,
            @Part ("images")String image);







}
