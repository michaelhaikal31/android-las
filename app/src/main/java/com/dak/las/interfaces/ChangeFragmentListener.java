package com.dak.las.interfaces;

import android.support.v4.app.Fragment;

public interface ChangeFragmentListener {
    void onChangeFragment(int fragment);
}
