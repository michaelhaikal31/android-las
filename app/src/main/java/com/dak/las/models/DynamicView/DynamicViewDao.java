package com.dak.las.models.DynamicView;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;



@Dao
public interface DynamicViewDao {

    @Query("SELECT * From DynamicView")
    List<DynamicView> getAll();

    @Query("SELECT * FROM DynamicView WHERE viewId=:id")
    DynamicView getView(int id);

    @Query("SELECT * FROM DynamicView WHERE viewId=:id AND terisi=1")
    DynamicView getFilledView(int id);

    @Query("SELECT * from DynamicView WHERE jenis=:jenis")
    DynamicView getId(String jenis);

    @Query("DELETE FROM DynamicView")
    void clearDynamicViewTable();

    @Insert
    void insertAll(DynamicView... DynamicView);


    @Delete
    void delete(DynamicView DynamicView);

    @Query("SELECT COUNT(viewId) FROM DynamicView")
    int getCount();
}
