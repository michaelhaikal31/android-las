package com.dak.las.models.SuperDialog;

import android.widget.RadioButton;

public class InputRadio {
    private RadioButton radioButton;
    private int postition;
    private int groupPosition;

    public InputRadio(RadioButton radioButton, int postition, int groupPosition) {
        this.radioButton = radioButton;
        this.postition = postition;
        this.groupPosition = groupPosition;
    }

    public RadioButton getRadioButton() {
        return radioButton;
    }

    public int getPostition() {
        return postition;
    }

    public int getGroupPosition() {
        return groupPosition;
    }
}
