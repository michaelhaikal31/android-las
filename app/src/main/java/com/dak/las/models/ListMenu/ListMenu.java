package com.dak.las.models.ListMenu;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dak.las.R;
import com.mikepenz.fastadapter.items.AbstractItem;


public class ListMenu extends AbstractItem<ListMenu,ListMenu.ViewHolder> {
    private int id;
    private String header;
    private String desc;
    private String to;
    private Boolean jenis;
    private Boolean mandatory;

    public ListMenu() {
    }

    public ListMenu(int id, String header, String desc, String to, Boolean jenis, Boolean mandatory) {
        this.id = id;
        this.header = header;
        this.desc = desc;
        this.to = to;
        this.jenis = jenis;
        this.mandatory = mandatory;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setJenis(Boolean jenis) {
        this.jenis = jenis;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getHeader() {
        return header;
    }

    public String getDesc() {
        return desc;
    }

    public String getTo() {
        return to;
    }

    public Boolean getJenis() {
        return jenis;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    @Override
    public int getType() {
        return R.id.item_1;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.row_menu_with_arrow;
    }

    @Override
    public void bindView(ViewHolder holder) {
        super.bindView(holder);
        holder.header.setText(header);
        //holder.desc.setText(desc);
//        if(mandatory){
//            holder.info.setVisibility(View.VISIBLE);
//        }
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder{
        final TextView header;
        //final TextView desc;
        //final ImageView info;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.txtHeaderRow);
            //desc = itemView.findViewById(R.id.txtDescRow);
            //info = itemView.findViewById(R.id.imgInfoSurvey);
        }
    }

    @Override
    public String toString() {
        return "ListMenu{" +
                "id=" + id +
                ", header='" + header + '\'' +
                ", desc='" + desc + '\'' +
                ", to='" + to + '\'' +
                ", jenis=" + jenis +
                '}';
    }
}
