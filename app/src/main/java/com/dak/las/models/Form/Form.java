package com.dak.las.models.Form;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.dak.las.models.BaseResponse;

import java.util.List;

@Entity
public class Form {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name="id")
    private int id;
    @ColumnInfo(name = "formId")
    private int formId;
    @ColumnInfo(name = "surveyId")
    private int surveyId;
    @ColumnInfo(name = "jsonForm")
    private String jsonForm;
    @ColumnInfo(name = "mandatory")
    private Boolean mandatory;


    public Form() {
    }


    public Form(int id, int formId, int surveyId, String jsonForm, Boolean mandatory) {
        this.id = id;
        this.formId = formId;
        this.surveyId = surveyId;
        this.jsonForm = jsonForm;
        this.mandatory = mandatory;
    }

    public int getId() {
        return id;
    }

    public int getFormId() {
        return formId;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public String getJsonForm() {
        return jsonForm;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }

    public void setJsonForm(String jsonForm) {
        this.jsonForm = jsonForm;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }



    @Override
    public String toString() {
        return "Form{" +
                "formId=" + formId +
                ", surveyId=" + surveyId;
    }

    public class ValueForm extends BaseResponse{
        private List<Form> surveys;

        public ValueForm(List<Form> surveys) {
            this.surveys = surveys;
        }

        public List<Form> getSurveys() {
            return surveys;
        }
    }
}
