package com.dak.las.models.SuperDialog;

import android.widget.AutoCompleteTextView;

public class InputAutoCompleteText {
    private AutoCompleteTextView editText;
    private int position;

    public InputAutoCompleteText() {
    }

    public InputAutoCompleteText(AutoCompleteTextView editText, int position) {
        this.editText = editText;
        this.position = position;
    }

    public AutoCompleteTextView getEditText() {
        return editText;
    }

    public int getPosition() {
        return position;
    }
}
