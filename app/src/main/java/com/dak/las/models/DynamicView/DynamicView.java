package com.dak.las.models.DynamicView;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity

public class DynamicView {

    @PrimaryKey
    @ColumnInfo(name = "viewId")
    private int viewId;

    @ColumnInfo(name = "jenis")
    private String jenis;

    @ColumnInfo(name = "view_title")
    private String viewTitle;

    @ColumnInfo(name = "json")
    private String jsonView;

    @ColumnInfo(name = "to")
    private String to;

    @ColumnInfo(name = "from")
    private String from;

    @ColumnInfo(name = "tukangen")
    private String tukangen;

    @ColumnInfo(name = "url")
    private String url;

    @ColumnInfo(name = "terisi")
    private Boolean terisi=false;

    @ColumnInfo(name = "mandatory")
    private Boolean mandatory;


    public DynamicView() {
    }

    public DynamicView(int viewId, String jenis, String viewTitle, String jsonView, String to, String from, String tukangen, String url, Boolean terisi, Boolean mandatory) {
        this.viewId = viewId;
        this.jenis = jenis;
        this.viewTitle = viewTitle;
        this.jsonView = jsonView;
        this.to = to;
        this.from = from;
        this.tukangen = tukangen;
        this.url = url;
        this.terisi = terisi;
        this.mandatory = mandatory;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Boolean getTerisi() {
        return terisi;
    }

    public void setTerisi(Boolean terisi) {
        this.terisi = terisi;
    }

    public int getViewId() {
        return viewId;
    }

    public void setViewId(int viewId) {
        this.viewId = viewId;
    }

    public String getViewTitle() {
        return viewTitle;
    }

    public void setViewTitle(String viewTitle) {
        this.viewTitle = viewTitle;
    }

    public String getJsonView() {
        return jsonView;
    }

    public void setJsonView(String jsonView) {
        this.jsonView = jsonView;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getJenis() {
        return jenis;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTukangen() {
        return tukangen;
    }

    public void setTukangen(String tukangen) {
        this.tukangen = tukangen;
    }

    @Override
    public String toString() {
        return "DynamicView{" +
                "viewId=" + viewId +
                ", jenis='" + jenis + '\'' +
                ", viewTitle='" + viewTitle + '\'' +
                ", jsonView='" + jsonView + '\'' +
                ", to='" + to + '\'' +
                ", from='" + from + '\'' +
                ", tukangen='" + tukangen + '\'' +
                ", url='" + url + '\'' +
                ", terisi=" + terisi +
                '}';
    }
}
