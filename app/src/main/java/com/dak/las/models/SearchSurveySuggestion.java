package com.dak.las.models;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

import java.util.List;

public class SearchSurveySuggestion implements SearchSuggestion {

    private String nama;
//    private String id;
    private boolean mIsHistory = false;

    public SearchSurveySuggestion(String keyword) {
        this.nama = keyword;
//        this.id = id;
    }

    public boolean ismIsHistory() {
        return mIsHistory;
    }

    public void setmIsHistory(boolean mIsHistory) {
        this.mIsHistory = mIsHistory;
    }

    public SearchSurveySuggestion(Parcel source) {
        this.nama = source.readString();
        //this.id = source.readString();
    }

    public SearchSurveySuggestion() {
    }

    @Override
    public String getBody() {
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public static final Creator<SearchSurveySuggestion> CREATOR = new Creator<SearchSurveySuggestion>() {
        @Override
        public SearchSurveySuggestion createFromParcel(Parcel in) {
            return new SearchSurveySuggestion(in);
        }

        @Override
        public SearchSurveySuggestion[] newArray(int size) {
            return new SearchSurveySuggestion[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nama);
        //dest.writeString(id);
        dest.writeInt(mIsHistory ? 1 : 0);
    }

    public class ValueSearchSurvey extends BaseResponse{
        private List<SearchSurveySuggestion> data;

        public ValueSearchSurvey(List<SearchSurveySuggestion> data) {
            this.data = data;
        }

        public List<SearchSurveySuggestion> getData() {
            return data;
        }
    }
}
