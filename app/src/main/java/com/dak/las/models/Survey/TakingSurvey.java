package com.dak.las.models.Survey;

import com.dak.las.models.BaseResponse;
import com.dak.las.models.Form.Form;

import java.util.ArrayList;
import java.util.List;

public class TakingSurvey {
    private String id_survey;
    private Form data;

    public String getId_survey() {
        return id_survey;
    }

    public Form getData() {
        return data;
    }

    public class ValueTakingSurvey extends BaseResponse {
        private List<TakingSurvey> surveys;

        public ValueTakingSurvey(List<TakingSurvey> surveys) {
            this.surveys = surveys;
        }

        public List<TakingSurvey> getSurveys() {
            return surveys;
        }
    }
}
