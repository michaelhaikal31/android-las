package com.dak.las.models;

public class BaseResponse {
    private String rc;
    private String rm;
    private String msisdn;
    private int id;
    private int jumlah_form;

    public BaseResponse() {
    }

    public BaseResponse(String rc, String rm, String msisdn, int id, int jumlah_form) {
        this.rc = rc;
        this.rm = rm;
        this.msisdn = msisdn;
        this.id = id;
        this.jumlah_form = jumlah_form;
    }

    public String getRc() {
        return rc;
    }

    public String getRm() {
        return rm;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public int getId() {
        return id;
    }

    public int getJumlah_form() {
        return jumlah_form;
    }
}