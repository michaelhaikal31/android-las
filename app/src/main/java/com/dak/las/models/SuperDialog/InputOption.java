package com.dak.las.models.SuperDialog;

import android.widget.Spinner;

import java.util.List;

public class InputOption {

    private Spinner spinner;
    private int position;
    private List<String> label;

    public InputOption(Spinner spinner, int position, List<String> label) {
        this.spinner = spinner;
        this.position = position;
        this.label = label;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    public int getPosition() {
        return position;
    }

    public List<String> getLabel() {
        return label;
    }
}
