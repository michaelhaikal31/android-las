package com.dak.las.models.Survey;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.dak.las.models.BaseResponse;
import java.util.ArrayList;

@Entity
public class Survey {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "idSurvey")
    private String id;
    @ColumnInfo(name = "nama")
    private String nama;
    @ColumnInfo(name = "flapon")
    private String flapon;
    @ColumnInfo(name = "produk")
    private String produk;
    @ColumnInfo(name = "alamat")
    private String alamat;
    @ColumnInfo(name = "cabang")
    private String cabang;
    @ColumnInfo(name = "status")
    private Boolean status;



    public Survey(String id, String nama, String flapon, String produk, String alamat, String cabang) {
        this.id = id;
        this.nama = nama;
        this.flapon = flapon;
        this.produk = produk;
        this.alamat = alamat;
        this.cabang = cabang;
        this.status = false;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getFlapon() {
        return flapon;
    }

    public String getProduk() {
        return produk;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getCabang() {
        return cabang;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "nama='" + nama + '\'' +
                ", status=" + status +
                '}';
    }

    public class ValueSurvey extends BaseResponse {
        private ArrayList<Survey> data;


        public ValueSurvey() {
        }

        public ArrayList<Survey> getData() {
            return data;
        }


    }


}
