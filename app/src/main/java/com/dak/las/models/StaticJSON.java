package com.dak.las.models;

import org.json.JSONObject;

public class StaticJSON {
    private JSONObject jsonObject;

    public StaticJSON(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public StaticJSON() {
    }

    public JSONObject getJsonObject() {
        try {
            jsonObject = new JSONObject("{\n" +
                    "  \"rc\": \"00\",\n" +
                    "  \"data\": {\n" +
                    "    \"form\": [\n" +
                    "      {\n" +
                    "        \"view_title\": \"Form biodata pengaju\",\n" +
                    "        \"element_form\": [\n" +
                    "          {\n" +
                    "            \"input_type\": \"string\",\n" +
                    "            \"length\": 15,\n" +
                    "            \"id\": 1,\n" +
                    "            \"label\": \"nama\",\n" +
                    "            \"type\": \"textfield\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"length\": 0,\n" +
                    "            \"options\": \"[{\\\"option1\\\":\\\"Laki - laki\\\",\\\"option2\\\":\\\"Perempuan\\\"}]\",\n" +
                    "            \"id\": 2,\n" +
                    "            \"label\": \"  jenis kelamin\",\n" +
                    "            \"type\": \"radio_button\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"input_type\": \"string\",\n" +
                    "            \"length\": 50,\n" +
                    "            \"id\": 3,\n" +
                    "            \"label\": \"alamat\",\n" +
                    "            \"type\": \"textarea\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"length\": 0,\n" +
                    "            \"id\": 4,\n" +
                    "            \"label\": \"foto user\",\n" +
                    "            \"type\": \"image_upload\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"length\": 0,\n" +
                    "            \"id\": 5,\n" +
                    "            \"label\": \"foto rumah user\",\n" +
                    "            \"type\": \"image_upload\"\n" +
                    "          }\n" +
                    "        ],\n" +
                    "        \"from\": 1,\n" +
                    "        \"id\": 2,\n" +
                    "        \"to\": 1,\n" +
                    "        \"url\": \"save\"\n" +
                    "      },\n" +
                    "      {\n" +
                    "        \"view_title\": \"Data pekerjaan pengaju\",\n" +
                    "        \"element_form\": [\n" +
                    "          {\n" +
                    "             \"input_type\": \"string\",\n" +
                    "            \"length\": 15,\n" +
                    "            \"id\": 1,\n" +
                    "            \"label\": \"Nama pekerjaan\",\n" +
                    "            \"type\": \"textfield\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "             \"input_type\": \"string\",\n" +
                    "            \"length\": 15,\n" +
                    "            \"id\": 2,\n" +
                    "            \"label\": \"Alamat tempat kerja\",\n" +
                    "            \"type\": \"textarea\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "             \"input_type\": \"number\",\n" +
                    "            \"length\": 2,\n" +
                    "            \"id\": 3,\n" +
                    "            \"label\": \"lama bekerja (tahun)\",\n" +
                    "            \"type\": \"textfield\"\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"input_type\": \"string\",\n" +
                    "            \"length\": 15,\n" +
                    "            \"id\": 4,\n" +
                    "            \"label\": \"pangkat\",\n" +
                    "            \"type\": \"textfield\"\n" +
                    "          },\n" +
                    "           {\n" +
                    "            \"length\": 0,\n" +
                    "            \"options\": \"[{\\\"option1\\\":\\\"Kontrak\\\",\\\"option2\\\":\\\"Pegawai tetap\\\"}]\",\n" +
                    "            \"id\": 5,\n" +
                    "            \"label\":\"Status kerja\",\n" +
                    "            \"type\": \"option\"\n" +
                    "          }\n" +
                    "        ],\n" +
                    "        \"from\": 1,\n" +
                    "        \"id\": 3,\n" +
                    "        \"to\": 1,\n" +
                    "        \"url\": \"save\"\n" +
                    "      }\n" +
                    "    ],\n" +
                    "    \"menu\": [\n" +
                    "      {\n" +
                    "        \"view_title\": \"Ini menu 1\",\n" +
                    "        \"list_menu\": [\n" +
                    "          {\n" +
                    "            \"header\": \"Profile pengaju\",\n" +
                    "            \"description\": \"-\",\n" +
                    "            \"to\": 2\n" +
                    "          },\n" +
                    "          {\n" +
                    "            \"header\": \"Data pekerjaan pengaju\",\n" +
                    "            \"description\": \"-\",\n" +
                    "            \"to\": 3\n" +
                    "          }\n" +
                    "        ],\n" +
                    "        \"from\": 0,\n" +
                    "        \"main\": true,\n" +
                    "        \"id\": 1,\n" +
                    "        \"to\": 0\n" +
                    "      }\n" +
                    "    ]\n" +
                    "  },\n" +
                    "  \"rm\": \"Success\",\n" +
                    "  \"msisdn\": \"62895376559480\"\n" +
                    "}");

        }catch (Exception e){

        }


        return jsonObject;
    }

    public static String base64="/9j/4AAQSkZJRgABAQEASABIAAD/4gIcSUNDX1BST0ZJTEUAAQEAAAIMbGNtcwIQAABtbnRyUkdCIFhZWiAH3AABABkAAwApADlhY3NwQVBQTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLWxjbXMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApkZXNjAAAA/AAAAF5jcHJ0AAABXAAAAAt3dHB0AAABaAAAABRia3B0AAABfAAAABRyWFlaAAABkAAAABRnWFlaAAABpAAAABRiWFlaAAABuAAAABRyVFJDAAABzAAAAEBnVFJDAAABzAAAAEBiVFJDAAABzAAAAEBkZXNjAAAAAAAAAANjMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB0ZXh0AAAAAElYAABYWVogAAAAAAAA9tYAAQAAAADTLVhZWiAAAAAAAAADFgAAAzMAAAKkWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPY3VydgAAAAAAAAAaAAAAywHJA2MFkghrC/YQPxVRGzQh8SmQMhg7kkYFUXdd7WtwegWJsZp8rGm/fdPD6TD////bAIQABQYGBwkHCgsLCg0ODQ4NExIQEBITHRUWFRYVHSsbIBsbIBsrJi4mIyYuJkQ2MDA2RE9CP0JPX1VVX3hyeJyc0gEFBgYHCQcKCwsKDQ4NDg0TEhAQEhMdFRYVFhUdKxsgGxsgGysmLiYjJi4mRDYwMDZET0I/Qk9fVVVfeHJ4nJzS/8IAEQgBXgIOAwEiAAIRAQMRAf/EABwAAAIDAQEBAQAAAAAAAAAAAAMEAQIFAAYHCP/aAAgBAQAAAAD3VzWuW9pgCSA2XT2rSykfQ247K+a6umydgl4CuCnXgc8Ob3IS9q1t1vab4sj59Xzxr3LexCFqurxy25YFqa/tyQBH4Vu6rzrN7jACLz1a04hZ69zTSLUrGk558A8O9jGvcx2S8GpLVoktN3veW4fnfiW3raTjE9SlLG4Yq1IYk0grV+rA6DAKKixmuqwUrTbRyVH00AovWben9L2Z8pz9B91u08OnXLYQRzdjeJkJkcPao6UEutUC/odk/jMk7Lrzp71HWQgCFcVRXqgUzDTBejqC6xb1COpPda1ReMRdcvWlR0UWXRX960dP5ydp/QePao6jCAAgrLB4hmWDntbq1oKtyXkI6N/SJgWX415wt6joIKqecj9GaMP5aZt1945IGMIFlRBVTGw242YpbWrLkZqEmLIxjN9MvWifhDus3itKiWRy836a6bJ+bc6664wWYEBZPOCuioTQ0dFwxLWmNq4AYaBTXAOL+w9LWmH5GzbZZ6K0Bn5mT9Vc759kJsuONsMGsNZVDKVTyws6m3ptnL0cTcEICHn7sXVrJ2d/UQ82HmGTW6YgKmdk+xaw1Vc4pmGGm2z8ukgikuinOhu6rxprHdv0EDM8zZgy3EYbNegKda5Zi15Gqlm6LXCz0yWjmXn2zUAquJdTPWb0dRw0ViKazw1PI5lmiisV/RYsNVet636nXvVdVZl26eZUj8hUtp6DZKirRcCKLxeZN0zNanplZ6gzscRjS1WrjTTWJHDpM9UVQGPddcTxai7Jd1H257oGBTPMRe95sUkxUS2cgpZlgzWprszRVUNRVGOe6A0Gc8hBDd6gFl31tN015gdF86jKkkuS5CdwU87OTkzuy+dlk1QcNdZUNL9FVwUaLwBWPNQlxlNTU0i361ahUqZYhSXLe8wJLMzVZN6TRIU4is36i6aidSW6M9WGCTUVWB39rsK5Xg2X4rcvVpUlpJe9ikJai6WbnL2N62x+67BzzcWeigM5LDy1KsFLw1al9l6IcG8/83amjDRhVY0WQ3m0zNr8umhmrGa9VFqNHGdmtxJqJBve9MxATJydRXLt9VYKQlvhd6E9X7o/k8n0jdpzSlGlSbWCsqoEh9F0dGGDs1HSVhLhpN6I5lb3v1QZ5PqDJiEv8DH3r/u093gDMW6gKyll9aaiAPokcwBrdbvQKvdUQ6WgKedI6zWo07fUdApC4nxytPtv0Du7MwWr1helBYFKxao+JIgKKB0dtoi9hApYg6yJVJEyqwRUpQvqvoJi1+RYwV/vXvO7snKZvWtaDB54QR2t0ksJbPr9G9NXC8acogr0Zpa9VsvOaxs7MTCc7mps7tfLJVWT91+gu7vHB0bLlbiMXGAEclJaYGmt6z31ah8PmsTRRAxisXXyVWvA4+Tt6CmHqazrPBGMSgfo31M3mc/SaJe5ZWRzFF+JcpLcFdb3fqOov5HM0SjUzwEYcMunRf5Jg6m+yfz+PramgYIRUWWl/X2tjRYOc3cFTMVVk5y2vNAL+k9vFAeMjTOBTPWsw80FUCnxDzHrNU17+F0tvTZEuKBig+tq6DTDbLRuAnmIq2aaYJa3DDT2G6PzuboPFAqkrzp24UXB8L8f6LZaITzXa+m0NQFy2PqEK0t2k625IEU81aWnWiXvaKiECuhuahuComvY0NMAVD8N8tPqDuEyC6T7AVBnaZ1OHSAJaD77sLrqIBqdplghSkmBqT6H2loyFFlR91Tt0Ep8V894v2Wm+cGtptlCrDOhotDQXCqibTdbgAE1hUsyywcp2CzRe/0Q/chjKh4cVOebh+MYvj8Df2tfa2dVq4g820yTls8C+abVuSggrhEODHZYaZMYkjY9/wB3IYQ6joCZsyanyTK8xg6D+9t6mo8WKQyU0jXQApmbG9iTQIxCELpIVl5opCFtb6ETuw87oiFVxkasL5uhioOGf1XdPQMWaGJfhqoo5AdzVTQmgqUpQVa3bdbMSTHvrerZxMib3vRMMRai/jVcwDEsund1GL3i5IoFbIRREVkxSxWtBjGIcGbbYMa5Tsmva1zmkalZGNZPCWRWKex7sPslJ0kqIGcmotQjrRD2qIABUEKsMMsMsEOy6YlR3bYrUdZqsjmqKoKXctbrtNELelRADRdUVCamo5a4Vk0gDqEcXYOwwdl95iwwBKaeuQ0p5mWFLPXOySKyQ1zFoIYA3lYdT62xuN8nlZuYqeqS1ZIYxm9LQ0GpEnngsS7TzVlczJWzkKMF64x3KQhpGNemv1lFZ2fR7r8q42HhIVWHWJve5dXW0NE9gZuPmUKfS1nDBzchLKQoyTr0FQrRrzAxO3DJQRr+p3XJHleZ8wgsKInrXsxt7Gg6bl8nCw0ht7+3oNjz8qnm0AlNbq3pzLMdWpNAYKmrOn6jedkeX5fzKCwq9PWJZzf1XXCQHI8/5vE5j0u/qN0SzN7wmWM9YpVg1bs9SlbM6ABSAmh6H0GhK+X5vz6ABx09cl3dtt9wlhY+D57Lhna3NFiFc32PjcxekiDQnEYPAF5uVhgKMmd1NvQuHMwspMQ4mZk1zPtaD5Djzs7MSH2m2ZqFc32GOmqtUtCgWrc5Es+rL5hLB47LbTJhqKLLhHXptYlQEcdeeciFhAXq2Q11Esv2ARAVqxa1lA1iEsld3UZEssvLTFr91ooIVK0m0iWQXbaf1XnWSXGvzrRx+dzcf2snsvnFPNhR1V0MoLL5wJI9JTkmIt1YFEDiwE83GzCOaetreg0tJkQxvXOv5bIxvaMMSRBd8l+tIVlUV2DQtmZhHWzXqEFYi3WilRJ5nns0BmGX930fqdNuRA0JHn+VxM//xAAbAQACAwEBAQAAAAAAAAAAAAACAwEEBQAGB//aAAgBAhAAAAChEQTTdynad08qY7u4p4YiCMQrUIGbtxrF1Ctu6o2RDigjEC7oWperXwrGnZOFLgOJzJBXHBFUY3hgFI1K+Pcvv7uUmubmsLKp6lueikbS6ICpYzIvWmlJV6tWy9pR52rsaLJNQiRyIVqgXOrXbLZ5dStdOTzql6WsnoBrjSlFdkZVC16KwUwrPsmxhhTQxXNbzLzqVZeeYYtXT2bLCkEMJjTCmipnnbutZpWaNVeQ7su7YPhl81bUuewaqU5vWLDW6N2rWRkM5doeop15RPB2wyFBKFrKE3dGK9agXEfVaavQc0CROl3D0KVJoz0XOZVueo1/IUARXW/VvzwshfTwoVLYyfIzpbDtL6Pc814palhXPV0GxArM+AFCasP53o7vo77fV6vlsXloz8+ufptIhgWMmBBKB854m1u+r0h5gv7qWTRR29efEDLHMEKqOp5ufo7luU8ywTKmbQG1ZcwJnpe6eBJCoSNsriCYyalVnKa5ZBHE1x9IrDp42LMEwbirZLLVogCaDTNru7q6JeTGLLgSLGVMBuzpURYyoBMbZGCq0evsNoQMJXMdmFrWoGFVwJjrkDKc4bjutEEhUSSTnjl5VgWJNuazK9WvVTXvzZoHowsRQUAPFC4iS0PS7VupgYVfMXcL/8QAGwEAAgMBAQEAAAAAAAAAAAAAAgMBBAUGAAf/2gAIAQMQAAAAre9MBUpHfjmcqp19xsxHve9EyYCtV+zJwlKou6SOazK249kl6Jla2kyIM45G91QVkqm3Yr11muJNwx4ajk2jOZlvK6HRVkKGBNcWWrTHVaPL5wrC3WsFJFDa3W08VcACDAbVgFt72xy+DXR5kv8ACTPT0GlzUaWTVWoYPWpTEdBc5wUqdICKge2bkF2Gxk8MoQktrLCAEnODYKvUQnNRffN+LPbW+U5uVR4rddcKUb29F19fDyKtfEo6T/aYnv4+UOgcptbuSNGmgmnsdijMpVq+Tj2rJ6MkzKids0+2W+evjK0zMXj0CZbzsFFq4duwjNq3b5VfFdC2jlwmfQVpb72weGFjVzfoelx/LVfGU1asqTSZZmZ8d5CrN/UeqnYL63GX82xRkr+hkYlVQ2T94i8cOda3b1fHpXPqGty3z/Mixr7dmeDyZIJiPEXm6Qt2L8YGWzcuYmZXjU6a41HGZozBeAIJlu6sbWjGPXTbKvWENDf004WfXA496FhEssibZgUob4Y8M6FyiHkLKGHC1h45Ow6VjWQayYYgFjtVczSMwnXo11h5pN07y82tXovgjfCw1/pRfO+ftmhNiRWvzibr9JPH0q+VcY59gK7D7N3D55EvzyFawNs3eoZzNBFPxMffmpbWtkVx8JHMLq5ZXbtvRtXufzp6JHLnYmLARdOolh+AIz+DxwPru3tdFe57G//EACAQAAMAAgIDAQEBAAAAAAAAAAECAwAEERIFEBMGIBT/2gAIAQEAAQIAwAAYAP4KsjzpJYLBJCYRlGMHWOpr6nyM+PJwlsS2EqKK4YN3NGq1CxI9HCeSTgHAAAwejgOchvFeUII8jcYSMGcjBgwYMA6mbxaCwERLp0ooHHTxMwgQqy3VawaboyEEN2ZmJIIHGcnORir144GcBSOoUgjDkPNN+ivs8kjOwKkYMGKFVVCGRj8RL5fNkdGVVKaLqvBBXzu3KUVks1QDOMOEdQgUqVK8cKqr16kADEUp0CMvUqVYeiSeZ6lNdSCMXECIqBehQp16lWDKydQM8d5EL03fIeT2U1pwSSTVOoBUggAADqykEBQJzl4hvD7Gn1UIvXoUZehQo6leHwnU8aqfHy3jlxcQIJqqhOpUr14YMCrKVOFiybld/oshFIrJZhevUggqBwBwVZSvCr47R69aT39RMQKOpUp1KMjoysKYSiqqjy0AFxBPJhQBwQRhxsYlmJLlmLYFVFmJrMJ169SCCD6GAAEEEEaicAEMN6IxCnoAgggqyujrQNiABV3PSYmJkypU8nDhJLF2Lly9HagdSgRVUKFA44IWa6TadtckMGDcthw5F1bDhyoOIZsCMOH0QwdaCoYoFVR5y9QjIyMjK4YN2JJYsaM9C71pc2Sk2nkwoUADB7jJixObkRikElm7cjPD72HDnkdosroyt27c4ScfKZUUyYVbX3NqyqVZHV1dXVgThDBsoaYzUajgo8Xjk8GKPfMcJJbGO5ncOrsxYsCpXNbyw8vseYdyRiMrh+/fv2LMXyuVXX8u/mbV63AAxcV1qlEdWBOOHV1ojSeNNcyCQEcmFAHPPPM3J5YnPIULq6sWLdgVxcA4YEYPSt379w4fsS5c0CYMYEbLAAehRKzqjq3JDK02iYtGkHj85xlk2Vg3btzyTq7Jw5Wm3sllZXLlhgMlRBPoyOpHrkHnt2Dhy7M2MJlc4rQjhFWZDJSU6SdHVsIKlDNptKkWlGbIpDhy4bnD6Xaffs9MYh1cNyCuQKBRjBw4JBJ5Lc9uezNyScXASXBGIuEkgtknkyOPfBBVhRXWBb1yG5BBGHOCGVxTHwlXVgRiiCoRnBDhkaXQ4CxBznk52LdlIPLFjPAwYlm7btI2lWdFoHDdiSTj5TJlsJGDAAAB644YUygphwYuSE9NIrJAGDMSPmykOpDehh9uSeVIYuWJkeexcsmt5LVk8qSdCH+v1+n0+juxTKNgAAwYAABwcbKZXKYQABpR5GJhb6JTgKQUebK4fBgzr1ItnOAg8EEKSeZT0/Gheu34K+vGoubfcW+vf6GnIBUIFC9QAAAOrBsoKhwQoVVTgD0IKFb0A4oHFAoUAdWFgRgxQAVZXCWzxGqA7AKP0OijrR6rRXT02DESen/jMgnUL14GcgnGDrRaJ0SazQFcJgrErNOOAXDqyunQYM4YWVhwMU88k0ymRabAjAAHQn6F5nxnh9f8tX8vu+HWMNcTCNJoLq/531iOQQcbHx1efyVAutZs7ExdcA57FhU0ZjjjggDGyofAAQwbsWcvmtgC4uDFwtR+wb894yMfVJ7XjUQJ0KMhHGbMzgPPYlgQV+fz6sF2G3Dual0KEkszE9+zNhw4cJZnehnhbv8AQ0+jVZ1fTsMBBGef3SSVb8RD+PKSRVHHV1KFOtEIOE88n1wFKlWD43qGmiJU1NOSSwcPySMJbHLktMsxo1jsf6TsNVaeG8kjgg22fLeTIbA/4i38eWpLAvBw5wVKVxgQc5B46hQOGDh8nHxn5/qV3fFcKBjYyvk3JzgKUedgVTHpWtNp9sbabH2lSbankE/Q0/Sbe5wcfCPyPnFb35Lbi63N3eTdOrDZZwcbOoAzgLxjY4Kfn9TOCHzy8lPXoyVmVQjFAQJVKROvOVKbFkE9NNPZ8UrRM2RlxlKlSGV1Ofnv2Ot+pp+g3vPydGDhgVIqb0pQ0xsLdhgAwZwQwdev5/2cfPM5Oaz+TSedJlFVFRPm83kkmWz7NNOc1DLTymlGsnkUzhgcOEMrKokYvFoslFoHVgeSWxzUuxYFcGDAAOCrKV8LseiXzyLSRE6Mrq6FFVUnP5tN5hHTZFqapUg4TsTi8WQjGxs5OFTMJNJRkiutlslVotASWZ2qXYurKylf44IZVOj5cUeu55SU5zVOrK6upQKiKULqy9XlsjalpMhBAZfJyiklmAGDDhR0+ctVdYIpDUM7TqlEdXLs7s5oeQUKMHDAg4QQUZWYGCRmkOvDY4YFZK2Aoxwh1L3Tdn4bZ4QoGzbScZySQm6MnznARXXTWMzPqSTkqJRKKzMXdqMxOAqysjAhgwIzjqyuiT8doyhlI1Vi5OESxiCh55YOt02p2zS8rPZXZFJpKKQWXyeTRWKylrJPh5PNlIothITClXLszM7Ek8qyMrq4YMpUjOCGSaasfe6GZifQL4MTGdTw6WneXkYSyezr7MhBJJNBP5tMyEpSVFXgh1YNjigks5Z2Ls7MxJJJBVlZXVlKsGDhjhGt/O8ThBBBU4D9FZSC5ol57sP8sdaMIJFJiQUdTMz+aIFCkEPlA2UbNZJoXOEliWJw4c5DBxRHR1YMHFA/Ktq7HvcvyMI6lXBzhSrd2dhVLxGvPWEZTmEMSuDBnUqoAzli2WNHtVH17/6n2u4LEk4RwQRzyrIyOrBu3KkODpbkfJtubPke3I9cMGzr145ZmdlolJiSTac1ULkmRgwbsSh57FmdqUtR2JKYCCCMK9eCCDhBw5yro60VwwYMpQgj0CGVgVBxsYccEHHLOysrrwmdVAwYjTcMGDdg4qXNGpWjFycOJk0GuswnzMyjBsOHDhw4MRlZXVgwxSmKB6OcqyMGLgtnAUq4qaFg6uGCAKFAxcRgwcP2LFzRq8tjFgRwoik1EV1TrNF5OjAggqQRgxSCrKQUyQUBCpBHZarX6LTsuAMtFqKBw5fOEVcGEelIbsrhyxYsx+ik67T69EWCSlHXEDCuvWNEqGWcrklhwMGAghp5MSCIqlWVxRvqK/UURkxQRQVWqvlS5AQHORjZyHDcggnCGxslEDvzSLzmuuutOU1kZUjsRqlwS9Wb+BgwFMis1kiKBw4qbs1RcXSsXmysMcVFRUVJIZW55DMc5VlwYG7diSNdXJIZWYKuuNUSA9ONhNoVx8b+hg9REFms1UAHKZfNg1drf6I7MKyojKztU1zehYkkqyv2DFxjAYjBue3bsMkXBzlC5yB1mkwPLtdt1qlycP8AIweoCAQzKnnlzc7B2XaveLa1IvNg7u7VbzMro6BQnUnt2RucXB6JLc941xk69gyvF9asaihq9b12aVZiSfXPoYPUMm6UnRX+najXa+bKNESnOAiUoKM7vV/KGs3j8+OrT+RQAlSuDDjEsHBGJQbL7RoWRkpG0NkbR2X2LXvShbDh/kYPU3W07JVKigd3plEtA65gkZTBV1ZnpSlN2zq02mZhPn8vg2u0gmIzCmUcUmyZwwOHAyurJRdlNr/W209ndsYHD/IwFqfYbMthNqe0lxY05ZWm0XkkgqwXUKOaNVyTjBlZOioZqjI0jIy+JSq1UCWIAHDB8JVkcN3FPqKdyxJJw+x65aj0e42U212o7KbM9lah1BVgU6RhzNdpaGrVfuMAyutiZwAVKfP5NNksjp8kki4+ULP2CqowDggZ27c4fRw+uXd3re20dobSbk9+e9r7UXm086snRVYqvNmq9qUdXm4YMH21RlIAHUIVZWV0onzVevD5YsUKBVCdCGPYt27cg4cPpy7Wrs7BbhQgVYSmulsQyYUkOIgKSW3K2pahP//EADgQAAEDAwEHAwIEBQQDAQAAAAEAAhEDECEgBBIwMUFRYSJAcTKBEyORoQVCUsHRUGJysTNT4fD/2gAIAQEAAz8Azx8Xi8o1arWDqVTpUwxowP30Nq7M8RkAkfI4c+2bUYKVQw8YBPX/AO3bSoPJOSIA8n282B2keATfxb0lE1HDyVix9znQVtlIRvBw7OVaMUmz8qrXfvPdJ9z+HXY48pz8FY0N2XYnun1uBaweT/hGUUfbzxtof9NNx+yrU/rYR7KLQmQKdQx2d/YoESDIRWy7K0l7pd0YMkqtttfffgDDW9AF4UdPZOc4AAknoFUIl53fHMox6X/qFVpH1N+/S08alSAJAc7uf7Wa5u64Ag9Chs1RrmfQ7l4Pb2ten9FRw+CtseINZ8fKJOUPaNo0g4j1uGfHiwTXNIIBB6I0X4+k8v8AHshU2Cp3b6h9v9LDq1MHq4aQ+i4dYkfZQbY4kWKDdjrk/wDrPtJ4ZcYARjJhN7pzc9FHA3Xtd2IKDmhw5ETowfgrPFzoDNl/D6vP7Bbp8H2EWkrPDDGjv10BpkcjwMoFoovMEfSf7aBTYWA+oj9Bxs3pUKZe8/A6lP2isXu+w7Beg+DxjKKKKM2ngzUaPOn8k+CNGNVZgAeN8futnIyHD7KQRTbHkouJJMk2zxMpwA32T5GE+PQwDycqpVfvPcSfNvQ7RHDKJvCzwd2o0+dIbRI7nRjUfY5WBo/l/XQ7sVnhC8LNiOCHDdccjl5uxjS5xgJ1V88gOQ0Y0TqjhBC0qLYUDHNEmxcYCaLNPMJzMgyOLlCLRwazREyPKreB9lUeZcSbRwhozx8aA0R+ugHC3KhbwRozaLlHhnRyUayjCI14UHgS8acqKrT3Fxwod7NziABJWPUfsFTHRDoiDmwtKHDzwPV9tO0P+mm4/ZbS2oJpuADecIixKPAlZUD2QZTDjzP/AFeECLGdE6M8aHBZtUq1A1oklUqLQSA53U/4QAUk+Vsu0CQNx/8AUP7hVdmrOp1BBH7+UBwJuTc8bIUADsNBRkkoNUqLYQ4GOEOR52DKW+R6nf8ASCnA5XFXZPxQPXTz8tR0kowiinE4CqnnAT+4KIMER7CEHMB7jQC74QlGUUIQ1xfCzY68qpvtAPMpzQB2EInmdAfSe08nNI/ULdeR2NyStq2rLGw3q44CotH5lQk+AqBbLKjh8hbRs59TZb/UOSJcAOqbTbgZ6mxQcIITg7dX9RVPyiPpMqOIB6Ty6aAN4DneBaCsoKeDlTryh+Mw/wC4LNjcNY4noCf0Uvce5NpKO2bUGn6G5cfCZTY1rWgNAgAXa9pa4Ag8wUNn2wwPSRLdMZ0SN4c+vFe3HML/AGpx5YQD4PXFhaFKIOvCxYLKxqCyoIQq0GPHUZ+dI2f+HvE+up6W/fmb5UbHVq9XPA+w0g0weoP/AHwZY7445lbXUyyk9w7gLaabR+LScPJCHdDuiUbFFTpKKIWVi2V5sO6nqs2FE7jz6HH9CmuAIIIPI3o0KRqVHBrR/wDoCftu0F5w0Ya3sL+pNf8AwwtnLahkfOndoDy4fsg4Ai8ZKnOjdYfjjOe4ADJVCi0PqtDn84PIIAQOSBVKoCWANd45FODiDgg5ti8LOqBbKJGBaFHVeUSedp62wtoofQ8x2OQqoGabT+y2iPTTYPPNbRtD96o8u7dh8aDKbsW2blQxSqYd47FBwBBkHrobWrBrfpb17lFvJDqE3oE5y6G4Ayt7lyCzxMprq2+Rhgn76MINrhw/mH7olGxRRBR1ElF2AgwQLQn1qkD7lUgB6d49ymEf+MfoiGl1MHH8v+Ed7PDgqtsbBRrg1KQ5H+Zvwv4TVaCK4b4cIX8NaJ/HB8NynVwWUwWsPM9TrI5FORPM2zxMoCnU+Rowh+X8lTpzwIBKyudtymB1OSg0ebgfnUx/yH9104p4mVjh5Qp1t0nDhH30/ibRA5NEfe2FjWUYthYKyi0FTUA8qXfCk39BBRo7S5o5TI+CiY4bpwE/4Th1RHCzbPDLSmOaG1TBH83dMcJDgfgpjRLnADyUHSykfl3+OEGjPO/WwBWVgqHSOhWHH40ekr81h/2owL4sdEovPhbogBFR0Qjkt3I5LOjGjHGKeORITjzJNieQlPHNp1y740S03jnaAUHONNxz0UWlTAW/Wj+kQuXACnot1vK/lHoj1Ra6NGNGPYZRquk4aOZTKbYAizXBFro05Pws39JWLZXNSCE+jWDmkgg4Kp1qY3sO6qkD9SB+lBrZPPoiTPU2xrHVAdLA9EQeSHZDssr1COyNo4osdUkBCnSa0dBnQIBFjfddOjppwpBwnsfIwqgAloKcQIEJzjJ4PXVlFFepEnAT+yIcZ9qPxWf8hpG4PnTKITUBy1SpCzyXQhQeFAGrKEIQpdYATKJeflEezlQQQm1KTXDqP0OgOfA5DjyhPK0OUcHA0A29SAUnwgDlUseoKhGXBUGg/htz3KJ9idD6DsZB5hbO4D1bvyqAE77f1QILWfqp04tOnN8rNs2i2bDR00wCSU4klE/6JI150ZtjhjrcDmUXHxq3kehRBzY8U8URYa86crHEI6p3dTqJKMC28OSf2Tu2s+xg6M6MXzfPsi7ABKq/0p45gjRhTCA6X8KLZWUOZTIwE0jkhrngwvN51Z9oaj46dUGCGiLAhDmOWibBC+UJKgpyJ4Em+dUaJ150ZQ1QeDDPk6YJCgrIWAhFxCCiVJPDzwoUWkrlqyjRqOafse4UE2zbKzeVlRrlDcCN5XrKyshYCwL4WFMrJ4o4ESjKlEO15QNNjuoMIorN4tmw4O6c8igbFBgnr0tJWdAtgrms8SBozoJRJRlFZUKNOUPwWju5C0WmxRCKIU6c6HN+E3qCm9AiTJKlRfyh3XlDvaeIBoCzebTfKhRpyhUfjkMBTqygQiEUR0RFsWzadMaIRHVeV5XlE9VPCi0G3mw7rOiSpti73cgVU7ItGb54QQ7W8WNjKMoqQsaSNBRRPDi2FHVZ5oDqvK8rypUrpfwpUIAbzvsETYFhPUXzaSmhMPMBAiW/pw8I9rFQsaAVKjgDhQgCcp+9zVTunwhhAjmgTzRKKlYvvVAFJtAXod8XyijchAEOHXhBBCUFi8I3EaijwiE6YCKzo3nAShTiE5wgqRlRYFQF+b9igLltI+cIo5Ulf//EAB8RAAIDAQEBAAMBAAAAAAAAAAECAAMRBBIQBRMUIP/aAAgBAgEBAgA/4CCoUjn/AE+FXlX2JpOTd+D7sIEBYFjACsVUrWsV+WDKorb0bQoXPJAmQAKRgGZjQ/EToryta1C/CpTyZ6BWZjAwDCALjqkfMwhlMY2WLEVP8EEsxYQAQfLun+imwTALli/5xg4a/os51UKwb6wdcQCCAvGLHiIb0CCyZB8zDHhHhFez+qu0HfjK6I4O7LuQcPlWDeg/smLMKmEtFOlrLHt4uhHB9brzBARB8YOIC/SlwcHa2UOH+NFf05sdK+bjQhw/v0SUwQQfGNnzqcRWFqMCspjiwEELBDLqKqB8FhtW5bbLP312noSwEFo8A6KvIUKpDK1FhayGEibCBM0urARhmFUKkEkzMak1qoDwXU2ra9rWex9wMXssY1v8FP6zUQlYHzIY02YRcKur+r+oWWuE/H/i7Pw/VyEEMpVkoNSiZhrVT/jI4+LGnW1vT/ZTZziyupeJWn5pGBhGFWtr7qexXBhbdEzDCCma57X7LOaczc4KVz8Z33dff0+P1mtkssYtEfmtRyd+KQZmEP8AGnfL6educ8wWYG/eJnnzcllbL5Wqla23dJBDKZhFomW89v48/iqeKpVGEYsBHy1XV0roWgBV3foKsGhhU84rKFDUKwoGEeQAQwJFlJ5wmGKApUAjdBDA6DphO+vghhgUgzVYEywv11dACIX9a48KAQQRN3XYsGHwRvnowgzQ3vpLRJRAHgmRoPgijCMywOamX4IB4wwlm+emFnPTQiqxr/WYC/wRYsC+cy1XRFT5o+FfLKyeCDBMBLh0sdy3onBFFdVfO3M9DK0ZXYX1uxZnanoBMI8lSgXDMAwsSPonPVz8tVP81vH1czqZeBKIx//EACkRAAICAgEEAQQDAAMAAAAAAAABAhEQISASMDFBUQMiYXETgZEjocH/2gAIAQIBAz8A4NjLx+CnhJN+8J6Nvv0K7RSw2NYt8NcKVYpD7LfN42QgqiqFOD1tG+xWH89vpjFL32aeKi2xPeK7F8W20vBNPzYpxT4dUE15Xag/Y5KvQ+n+yubHfF9Dr4wx/wAf98Yv0JeOy2xRVEYlPwRkrT4IsofVT427j/hNv7mkhRSS8LOuO8a5UrPzj/kr5RfKp9jZRBfkUvfDZbRri2Ppf6xP6kqX9scZ9XV4/A15HhZ3fY2NRSXsbGno8F42WzXO9o6IJf6Uaxsvwmy/OhRX5PqCl50yCIy8Pl1RTXrOsUbNia52WsUm8Of1Ix+WJLWG5ZY4yT5wfoj0tLQ0tjNFZWFxoiN/hY6Pqxk/Ce83G/ZL4GleHJ9rQyinTF8ivyWSTosjKKclbe9+j6M41ST+Uif0fqOMl4/75TSpS18PZe274R+BLtaEVE6Z+Rjkh+xt2fchfxpr2URl9NS9r/3kofsafojLTE+Gu39rH/IfYpM3i0URS6JP9EEvI5rpvy7HmivA3hpjcU+5WNDp0Su2ioV8M2j7ctH1GqcmXw1iizZ0wSHeF2fGLFIUvQo7RSRS7NrLbEneK94srsJrYvkSwixLnWbPg3sSQjY2axa7iFyVctCimx3pFumhN4SEymasl1W2Vw1yvu3D+zeHa/RodjLRrjs1wrD4XHLHXFNUSvRLq2VjqZQkuWzXC1jfFZVYd4d5rttsdbNDQ1lQWybeqOpYpMfmx9XS+WzWELs20KrI/BBrwQOmynhua/RR9rf5Nn//xAAhEQACAgIDAQEBAQEAAAAAAAABAgMEABEFEBITIBQVBv/aAAgBAwEBAgD8EkyBoY6qUnr3kaGUwyKd771rsdEEZ8sZ1fCWYsSDHiGY3IjGaqqp3v8ALP8AUMDvoDRDPUlxiSxOAo4meYt8PmUA2ueSN7JUFCRgzfXpcjigqSKzH8etllZDjDAKnHNQtQYWLRk5Ji9Ajo5GYqHHUOUwkjRw42AtgxCCTqAKFHMEh1I0CFGHNgg7UwN/RZlhq/5M1cggr50ivUAzQyrycnLvIVK/NYTCUxj6V1bAdlkSvDHBzVNlIK+fKqrEEYc2CpzUHEyUmhdDFJHIY2ToYQBBldbN7k+ZEnnz50AshBBB6ULgzg4CJI2rzRsrLaCGIkgnpcrcjYvOxzwIzE0VOn/nWKS8bNXYEADN8Tb+skzSyAo8dqv84s0q55C+T1pUK9UyX9h50kw4MHcd4XBP6iz+axXlhSJIRCqpB/K0U0HyhgBfuG1/QtkPYts+/wAACNlSVZK81hHhFdI4IYI+O4uWhe4+VCQ3oMTI+/bSm5LNCfyhyTEwGF0K1nikMM9IwqcvpcD9jK9U8ZZ41g2FREE62D0JNriiMVQ7SCxiy15OIvzScnyE8zSK65DRWGMOt2FlA0c0R0Gw5DGAwjyu+mFtmyOStafkpp3ckGoY5AWLz25G70AQQc2DuscdldJo7sl13JVkkMruSc3WeJ42uW5bbgjWu9EEYCGFn2CD79EsQQxYnrSmK3/ovJ0xwHBnkqQRoDWlCqE+ZUhsB2SCM0QRuBIODs8PMmBfBVCbLucIIAA0qxQrWlgcNj5E+eQoAQqVKcQI8ZeTXEw45GLhw4cODB0hgesl2GYEsYwGBXABG6geYmqcta5azMQH9k6Xs4c9BwfUElazZmmxsbAhVWV0dJXcFQ2BhhiZPIUr5A3jZJM9yOythGTI5UxqVuDUSQRcjxxCkN62rswyOMhz6VfBB7tT3eRaZbzf9FxfLQuDR65Vhn//xAAuEQABAwMEAQMDBAIDAAAAAAABAAIRAxAhEiAxQVEwYXEEE5EUIlKhgbEyQtH/2gAIAQMBAz8A2hAppPCaek3wgETUjoWc0EjkcLWxrvIB9eQnAREjrynOdxA7UJjeSB8prhIII9twmwhBfu1flSvuHT12fb/1AD0QOSm+6B42hBYT3ulxJKNOoDOCc7oNoUqVTJ/4j8IDi0WJ3a3knqxaZ3yiUXva1vJMIsJB5G878pjWgvEnwqLxxB9kaTy09Wiwa8zw7/d8b6+Bp/tNo/udlx/pAVRH8d+LHaPusniRNx98DvSJsbSnRg4WZOzOwEDKYxsko1ahce1UqHGB5TiMOz7hVKTtLhB3ZQFMOG0NaG1AcdhUmtOgEn34Tqjy5xkk3koWi2LZ2EIlFzgPJQAAAwpCH6cP7af6KjblE0I8eiSYAyvqDkgD5T6fIWL4wiAVncBUaT5QwqX07ROXHgJ9Sj9vQ0Amfwg7EQU2EdhDCPRa6q95E6QI+SgE1wMoZCg2wolZ36Gw4Ex2F9x7nGSSi4yVGfCkXITqzj00clfTBsZ+ZRYRpyCqxEwB8p9Mw4RuFKqWkwHR+RYRyuVKkLCkSi03OzB+Lk4HJWAoE3DaOPJtkIJr6bgfCzu+oYIDzH5T9Yc90x5THmWkIISgQosQiVjhSiek4dIhEmW/5Cqfx/0tJ1O56Hi0iL6HEf8AVU/5JpfHXmzabDJyRhSdxJACA4UZ7RIyoIQPaDhIUnheyATS2VJCYKYc4ZKoubGkfKcwkfhFpO0I9bKowCU93JRI3Q4HbCLgpWlAKGgIahKH2mkeLNdQJPRQ1na6rk4Cpkdp9MFwyEQiiiVGN7gI5ROwLoWgqMFQU19IMccjhMHYTBT0A/Kkk3lF0ajCDQADwsoGfdBtVwHmw9LU72CA4Q5sFlYQ1fFoKc0YKrFsaiieSpv+8HwphSoAKOmZWqo4+Tc+iJItAtCI5RIgKbxtyoddrGAdlOeI4Cn1CDITvARcZNiiifShFvOVAwMp73STJRCxshT6RRtG3O91R4aBJJTA0a3kn2WlpLXSE5hg2m4mCqQpaGt/yVq9AEKVpCzedxsUP1PwMIFoQgoanR0Vmwtn0gYC8pulQbkE/NwhN82NN4cOlSLQHGFSFMhhknvwtZtARPoAIWyiCsAFft5Um8BHwioRlYTSFlCELTbOw7Wt5KBOLBA2hPqugKlpyTK+2+OjxYOcAmcQITBTNRgiORaL5WFJWUSERed2hpKeXFrefKq/zKr0zh0/Kq0yAWyvvgEBBzQbAUieyUSh9xrfDbf/2Q==";
}
