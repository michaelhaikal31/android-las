package com.dak.las.models.SuperDialog;

import android.widget.EditText;

public class InputText {
    private EditText editText;
    private int position;

    public InputText() {
    }

    public InputText(EditText editText, int position) {
        this.editText = editText;
        this.position = position;
    }

    public EditText getEditText() {
        return editText;
    }

    public int getPosition() {
        return position;
    }
}
