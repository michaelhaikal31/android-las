package com.dak.las.models.Form;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.dak.las.models.Survey.Survey;

import java.util.List;

@Dao
public interface FormDao {

    @Query("SELECT * From Form")
    List<Form> getAllForm();

    @Query("SELECT * FROM Form WHERE formId=:id AND surveyId=:id_survey")
    Form getView(String id,String id_survey);

    @Insert
    void insertAll(Form... forms);

    @Update
    void updateData(Form form);

    @Delete
    void delete(Form form);


    @Query("DELETE FROM Form")
    void clearFormTable();

    @Query("SELECT COUNT(formId) FROM Form")
    int getCount();

    @Query("DELETE FROM Form WHERE surveyId=:id_survey")
    void delete(String id_survey);

    @Query("SELECT * FROM form WHERE surveyId=:id_survey")
    List<Form> getAllForm(String id_survey);
}
