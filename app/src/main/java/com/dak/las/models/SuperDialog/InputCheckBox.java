package com.dak.las.models.SuperDialog;

import android.widget.CheckBox;

public class InputCheckBox {
    private CheckBox checkBox;
    private int position;

    public InputCheckBox(CheckBox checkBox, int position) {
        this.checkBox = checkBox;
        this.position = position;
    }

    public CheckBox getCheckBox() {
        return checkBox;
    }

    public int getPosition() {
        return position;
    }
}
