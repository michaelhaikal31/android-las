package com.dak.las.models.Survey;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.dak.las.models.DynamicView.DynamicView;

import java.util.List;

@Dao
public interface SurveyDao{

        @Query("SELECT * From Survey")
        List<Survey> getAllSurvey();

        @Query("SELECT * FROM Survey WHERE idSurvey=:id")
        Survey getView(int id);

        @Insert
        void insertAll(Survey... surveys);

        @Query("DELETE FROM Survey")
        void clearSurveyTable();

        @Query("DELETE from Survey where idSurvey=:id")
        void delete(String id);

        @Query("SELECT COUNT(idSurvey) FROM survey")
        int getCount();
}
