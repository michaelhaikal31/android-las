package com.dak.las.models

import com.google.gson.annotations.SerializedName

class DynamicResult {
    @SerializedName("view_title")
    val title : String= "view_title"
    @SerializedName("value")
    val value : String = "value"


}

class ValueDynamicResult : BaseResponse(){
    val data = ArrayList<DynamicResult>()
}