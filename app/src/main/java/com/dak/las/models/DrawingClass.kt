package com.dak.las.models

import android.graphics.Paint
import android.graphics.Path
import com.google.gson.annotations.SerializedName

class DrawingClass{

    var DrawingClassPath : Path? = null
    var DrawingClassPaint : Paint? = null

}