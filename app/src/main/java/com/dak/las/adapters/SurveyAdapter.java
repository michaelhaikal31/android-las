package com.dak.las.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dak.las.R;
import com.dak.las.models.Survey.Survey;

import java.util.ArrayList;
import java.util.List;

public class SurveyAdapter extends RecyclerView.Adapter<SurveyAdapter.SurveyViewHolder> {

    private Context context;
    private List<Survey> listSurvey;
    private String id_trx;
    final OnSurveyInteractionListener listener;
    final int jenis_row;

    public SurveyAdapter(Context context, List<Survey> listSurvey, OnSurveyInteractionListener listener, int jenis_row,String id_trx) {
        this.context = context;
        this.listSurvey = listSurvey;
        this.listener = listener;
        this.jenis_row = jenis_row;
        this.id_trx = id_trx;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public SurveyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_survey,parent,false);
        SurveyViewHolder holder = new SurveyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final SurveyViewHolder holder, final int position) {
        final Survey survey = listSurvey.get(position);
        holder.cabang.setText(survey.getCabang());
        holder.nama.setText(survey.getNama());
        holder.plafon.setText(survey.getFlapon());
        holder.produk.setText(survey.getProduk());
        holder.alamat.setText(survey.getAlamat());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(listSurvey.get(position).getStatus()==null){
                    survey.setStatus(true);
                }else if(survey.getStatus()){
                    survey.setStatus(false);
                }else{
                    survey.setStatus(true);
                }
               listener.onItemChecked(survey.getId(),position,isChecked);
            }
        });
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemChecked(survey.getId(),position,false);
            }
        });
        holder.btnTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.btnTwoClicked(survey.getId(),id_trx);
            }
        });
        if(jenis_row==1){
            holder.btnThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onViewInventoryClicked(survey.getId());
                }
            });
        }else{
            holder.btnThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.btnThreeClicked(survey.getId(),id_trx);
                }
            });
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.checkBox.isChecked()){
                    holder.checkBox.setChecked(false);
                }else{
                    holder.checkBox.setChecked(true);
                }
            }
        });

        holder.btnOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jenis_row == 5 || jenis_row == 6) {
                    listener.onViewInventoryClicked(survey.getId());
                } else {
                    listener.onKirimClicked(survey.getId(), position);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return listSurvey.size();
    }

    class SurveyViewHolder extends RecyclerView.ViewHolder {
        final TextView cabang,nama,plafon,produk,alamat;
        final CheckBox checkBox;
        final Button btnOne;
        final Button btnTwo;
        final Button btnThree;
        final ImageView btnDelete;
        public SurveyViewHolder(@NonNull View itemView) {
            super(itemView);
            cabang = itemView.findViewById(R.id.txtCabangSurvey);
            nama = itemView.findViewById(R.id.txtNamaSurvey);
            plafon = itemView.findViewById(R.id.txtPlafonSurvey);
            produk = itemView.findViewById(R.id.txtProdukSurvey);
            alamat = itemView.findViewById(R.id.txtAlamatSurvey);
            checkBox = itemView.findViewById(R.id.checkBox);
            btnDelete = itemView.findViewById(R.id.btnDeleteRowSurvey);
            btnOne = itemView.findViewById(R.id.btnOneSurvey);
            btnTwo = itemView.findViewById(R.id.btnTwoSurvey);
            btnThree = itemView.findViewById(R.id.btnThreeSurvey);
            switch (jenis_row) {
                case 1:
                    btnDelete.setVisibility(View.INVISIBLE);
                    btnOne.setVisibility(View.INVISIBLE);
                    btnTwo.setVisibility(View.INVISIBLE);
                    btnThree.setText("Detail Inventory");
                    break;
                case 2:
                    btnOne.setText("Kirim Survey");
                    btnTwo.setText("LTC");
                    btnThree.setText("LKA");
                    checkBox.setVisibility(View.INVISIBLE);
                    break;
                case 3:
                    btnDelete.setVisibility(View.INVISIBLE);
                    btnOne.setVisibility(View.INVISIBLE);
                    btnTwo.setVisibility(View.INVISIBLE);
                    btnThree.setText("Edit survey");
                    break;
                case 4 ://jenis row risk legal
                    btnOne.setText("Kirim Survey");
                    btnTwo.setVisibility(View.INVISIBLE);
                    btnThree.setText("Hasil Survey");
                    checkBox.setVisibility(View.INVISIBLE);
                    break;
                case 5://ongoing survey
                    btnOne.setText("Detail Inventory");
                    btnTwo.setText("LTC");
                    btnThree.setText("LKA");
                    checkBox.setVisibility(View.INVISIBLE);
                    break;
                case 6://ongoing survey
                    btnOne.setText("Detail Inventory");
                    btnTwo.setVisibility(View.INVISIBLE);
                    btnThree.setText("Hasil Survey");
                    checkBox.setVisibility(View.INVISIBLE);
                    break;

            }
        }
    }

    public interface OnSurveyInteractionListener{
        void onItemChecked(String id,int pos,Boolean checked);
        void btnThreeClicked(String id_survey,String id_trx);
        void btnTwoClicked(String id_survey,String id_trx);
        void onViewInventoryClicked(String id_survey);
        void onKirimClicked(String id_survey,int pos);
    }
}
