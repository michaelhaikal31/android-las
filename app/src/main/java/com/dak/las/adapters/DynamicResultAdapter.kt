package com.dak.las.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.dak.las.R
import com.dak.las.models.DynamicResult

class DynamicResultAdapter(private val list : List<DynamicResult>)
    : RecyclerView.Adapter<DynamicHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): DynamicHolder {
        val inflater = LayoutInflater.from(parent.context)
        return DynamicHolder(inflater,parent)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: DynamicHolder, position: Int) {
        val dynamicResult : DynamicResult = list[position]
        holder.bind(dynamicResult)

    }
}

class DynamicHolder(inflater : LayoutInflater,parent : ViewGroup) : RecyclerView.ViewHolder(inflater.inflate(R.layout.row_dynamic_result,parent,false)) {
    private var title : TextView? = null
    private var value : TextView? = null

    init{
        title = itemView.findViewById(R.id.txtTitleDynamicResult)
        value = itemView.findViewById(R.id.txtValueDynamicResult)
    }

    fun bind(dynamicResult : DynamicResult){
        title?.text = dynamicResult.title
        value?.text = dynamicResult.value
    }



}