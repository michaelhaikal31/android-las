package com.dak.las.adapters

import android.app.Activity
import android.os.Message
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.dak.las.R
import kotlinx.android.synthetic.main.row_notif.view.*

class AdapterNotif(private var activity : Activity, private var items : ArrayList<Message>):BaseAdapter(){


    private class ViewHolder(row : View?){
        var txtTitle : TextView?=null
        var message : TextView?=null

        init{
            this.txtTitle = row?.findViewById(R.id.txtTitleNotif)
            this.message = row?.findViewById(R.id.txtMessageNotif)
        }


    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItem(position: Int): Any {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getItemId(position: Int): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCount(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}