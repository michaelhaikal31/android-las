//package com.dak.las.adapters
//
//import android.content.Context
//import android.support.v7.widget.RecyclerView
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.Button
//import android.widget.CheckBox
//import android.widget.ImageView
//import android.widget.TextView
//import com.dak.las.R
//import com.dak.las.models.Survey.Survey
//
//class SurveyAdapterKot(val context: Context, var listSurvey : List<Survey>, val listener :OnSurveyInteractionListener,internal val jenis_row : Int , val id_trx : String) : RecyclerView.Adapter<SurveyAdapterKot.ViewHolder>{
//
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//       return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_list_survey,parent,false))
//    }
//
//    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//    }
//
//    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
//
//    }
//
//    internal inner class ViewHolder(view : View):RecyclerView.ViewHolder(view){
//
//        init{
//            val cabang = itemView.findViewById<TextView>(R.id.txtCabangSurvey)
//            val nama = itemView.findViewById<TextView>(R.id.txtNamaSurvey)
//            val plafon = itemView.findViewById<TextView>(R.id.txtPlafonSurvey)
//            val produk = itemView.findViewById<TextView>(R.id.txtProdukSurvey)
//            val alamat = itemView.findViewById<TextView>(R.id.txtAlamatSurvey)
//            val checkBox = itemView.findViewById<CheckBox>(R.id.checkBox)
//            val btnDelete = itemView.findViewById<ImageView>(R.id.btnDeleteRowSurvey)
//            val btnOne = itemView.findViewById<Button>(R.id.btnOneSurvey)
//            val btnTwo = itemView.findViewById<Button>(R.id.btnTwoSurvey)
//            val btnThree = itemView.findViewById<Button>(R.id.btnThreeSurvey)
//            when (jenis_row) {
//                1 -> {
//                    btnDelete.visibility = View.INVISIBLE
//                    btnOne.visibility = View.INVISIBLE
//                    btnTwo.visibility = View.INVISIBLE
//                    btnThree.text = "Detail Inventory"
//                }
//                2 -> {
//                    btnOne.text = "Kirim Survey"
//                    btnTwo.text = "LTC"
//                    btnThree.text = "LKA"
//                    checkBox.visibility = View.INVISIBLE
//                }
//                3 -> {
//                    btnDelete.visibility = View.INVISIBLE
//                    btnOne.visibility = View.INVISIBLE
//                    btnTwo.visibility = View.INVISIBLE
//                    btnThree.text = "Edit survey"
//                }
//                4//jenis row risk legal
//                -> {
//                    btnOne.text = "Kirim Survey"
//                    btnTwo.visibility = View.INVISIBLE
//                    btnThree.text = "Hasil Survey"
//                    checkBox.visibility = View.INVISIBLE
//                }
//                5//ongoing survey
//                -> {
//                    btnOne.text = "Detail Inventory"
//                    btnTwo.text = "LTC"
//                    btnThree.text = "LKA"
//                    checkBox.visibility = View.INVISIBLE
//                }
//                6//ongoing survey
//                -> {
//                    btnOne.text = "Detail Inventory"
//                    btnTwo.visibility = View.INVISIBLE
//                    btnThree.text = "Hasil Survey"
//                    checkBox.visibility = View.INVISIBLE
//                }
//            }
//        }
//
//
//    }
//
//    interface OnSurveyInteractionListener {
//        fun onItemChecked(id: String, pos: Int, checked: Boolean?)
//        fun btnThreeClicked(id_survey: String, id_trx: String)
//        fun btnTwoClicked(id_survey: String, id_trx: String)
//        fun onViewInventoryClicked(id_survey: String)
//        fun onKirimClicked(id_survey: String, pos: Int)
//    }
//}
//
