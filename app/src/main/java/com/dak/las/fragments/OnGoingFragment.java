package com.dak.las.fragments;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.dak.las.R;
import com.dak.las.activities.DetailInventoryActivity;
import com.dak.las.activities.SuperForm;
import com.dak.las.adapters.SurveyAdapter;
import com.dak.las.interfaces.ApiService;
import com.dak.las.models.DynamicView.DynamicView;
import com.dak.las.models.Survey.Survey;
import com.dak.las.services.RetrofitService;
import com.dak.las.sessions.UserSession;
import com.dak.las.utilities.Constant;
import com.dak.las.utilities.ConstantString;
import com.dak.las.utilities.Functions;
import com.dak.las.utilities.LasDatabase;
import com.dak.las.utilities.SuperDialog;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OnGoingFragment extends Fragment implements SurveyAdapter.OnSurveyInteractionListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;

    @Override
    public void onKirimClicked(String id_survey,int pos) {

    }

    private String mParam2;
    private ArrayList<Survey> surveyArrayList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int identifier=0;
    private LasDatabase db;
    private SurveyAdapter adapter;
    private RecyclerView recyclerView;
    private String account_type;
    private ImageView imageView;
    private final UserSession userSession = new UserSession();

    public OnGoingFragment() {
        // Required empty public constructor
    }

    public static OnGoingFragment newInstance() {
        OnGoingFragment fragment = new OnGoingFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        db = Room.databaseBuilder(getActivity().getApplicationContext(),
                LasDatabase.class, "db_las").allowMainThreadQueries().build();
        if(userSession.getAccountType(getActivity()).contains("risk")){
            account_type=userSession.getAccountType(getActivity());
        }else{
            account_type=userSession.getAccountType(getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_on_going, container, false);
        recyclerView = v.findViewById(R.id.rvOnGoing);
        swipeRefreshLayout = v.findViewById(R.id.swipeOnGoing);
        imageView = v.findViewById(R.id.imgOnGoing);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Functions.isNetworkAvailable(getActivity())){
                    getListOnGoing();
                }else{
                    imageView.setVisibility(View.VISIBLE);
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        //getDataSurfey();
        if(Functions.isNetworkAvailable(getActivity())){
            getListOnGoing();
        }else{
            imageView.setImageResource(R.drawable.ic_tidak_ada_koneksi);
            imageView.setVisibility(View.VISIBLE);
        }

        return v;
    }

    private void getListOnGoing(){
//        final SuperDialog progressBar = new SuperDialog(getActivity());
//        progressBar.createProgressDialog("Please wait..",getActivity().getLayoutInflater());
//        progressBar.show();
        imageView.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(true);
        final ApiService apiClient = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
        final JSONObject jsonObject= new JSONObject();
        try{
            jsonObject.put("msisdn",new UserSession().getMsiSdn(getActivity()));
            jsonObject.put(ConstantString.JSON_REQUEST_GET_SURVEY,1);
        }catch (Exception e){

        }
        Call<Survey.ValueSurvey> call = apiClient.getOnGoingSurvey(jsonObject.toString(),"{}");
        call.enqueue(new Callback<Survey.ValueSurvey>() {
            @Override
            public void onResponse(Call<Survey.ValueSurvey> call, Response<Survey.ValueSurvey> response) {
                try{
                    if(response.body().getRc().equals("00")){
                        surveyArrayList = response.body().getData();
                        if(account_type.contains("Risk")){
                            adapter = new SurveyAdapter(getActivity(), surveyArrayList, OnGoingFragment.this, 6, String.valueOf(response.body().getId()));
                        }else {
                            adapter = new SurveyAdapter(getActivity(), surveyArrayList, OnGoingFragment.this, 5, String.valueOf(response.body().getId()));
                        }
                        linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        swipeRefreshLayout.setRefreshing(false);
                    }else if (response.body().getRc().equals("20")){
                        swipeRefreshLayout.setRefreshing(false);
                        Snackbar.make(getActivity().findViewById(R.id.layoutMain),"",Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getListOnGoing();
                            }
                        }).show();
                        imageView.setImageResource(R.drawable.ic_anda_belum__memiliki_data_survey);
                        imageView.setVisibility(View.VISIBLE);
                    }else{

                    }

                }catch (Exception e){
                    swipeRefreshLayout.setRefreshing(false);
                    imageView.setVisibility(View.VISIBLE);
                    Snackbar.make(getActivity().findViewById(R.id.layoutMain),"",Snackbar.LENGTH_SHORT).setAction("Retry", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getListOnGoing();
                        }
                    }).show();
                }
            }

            @Override
            public void onFailure(Call<Survey.ValueSurvey> call, Throwable t) {
                imageView.setImageResource(R.drawable.ic_tidak_ada_koneksi);
                imageView.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);
                SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_CANNOT_CONNECT,Constant.DESC_CANNOT_CONNECT);
            }
        });
    }

    @Override
    public void onItemChecked(String id, int pos, Boolean checked) {

    }

    @Override
    public void btnThreeClicked(final String id_survey,final String id_trx) {
        new AsyncTask<Void, String, DynamicView>() {
            @Override
            protected DynamicView doInBackground(Void... voids) {
                if (account_type.contains("Risk")) {
                    return db.dynamicViewDao().getId("menu-risk-legal");
                } else {
                    return db.dynamicViewDao().getId("menu-LKA");
                }
            }

            @Override
            protected void onPostExecute(DynamicView dynamicView) {
                super.onPostExecute(dynamicView);
                Intent intent = new Intent(getActivity(),SuperForm.class);
                intent.putExtra(SuperForm.ARG_PARAM1,String.valueOf(dynamicView.getViewId()));
                intent.putExtra(SuperForm.ARG_PARAM2,id_survey);
                intent.putExtra(SuperForm.ARG_PARAM4,true);
                intent.putExtra(SuperForm.ARG_PARAM5,id_trx);
                System.out.println("asd data "+dynamicView.getViewId());
                startActivity(intent);
            }
        }.execute();
    }

    @Override
    public void btnTwoClicked(final String id_survey,final String id_trx) {

        new AsyncTask<Void, String, DynamicView>() {
            @Override
            protected DynamicView doInBackground(Void... voids) {
                    return db.dynamicViewDao().getId("menu-LTC");

            }

            @Override
            protected void onPostExecute(DynamicView dynamicView) {
                super.onPostExecute(dynamicView);
                Intent intent = new Intent(getActivity(),SuperForm.class);
                intent.putExtra(SuperForm.ARG_PARAM1,String.valueOf(dynamicView.getViewId()));
                intent.putExtra(SuperForm.ARG_PARAM2,id_survey);
                intent.putExtra(SuperForm.ARG_PARAM4,true);
                intent.putExtra(SuperForm.ARG_PARAM5,id_trx);
                startActivity(intent);
            }
        }.execute();
    }

    @Override
    public void onViewInventoryClicked(String id_survey) {
        final Intent intent = new Intent(getActivity(), DetailInventoryActivity.class);
        intent.putExtra(ListSurveyFragment.PARAM_ID_TRX,id_survey);
        startActivity(intent);
    }
}
