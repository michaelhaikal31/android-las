package com.dak.las.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.dak.las.R;
import com.dak.las.activities.DetailInventoryActivity;
import com.dak.las.activities.SearchActivity;
import com.dak.las.adapters.SurveyAdapter;
import com.dak.las.interfaces.ApiService;
import com.dak.las.interfaces.ChangeFragmentListener;
import com.dak.las.models.Form.Form;
import com.dak.las.models.Survey.Survey;
import com.dak.las.services.RetrofitService;
import com.dak.las.sessions.UserSession;
import com.dak.las.utilities.Constant;
import com.dak.las.utilities.ConstantString;
import com.dak.las.utilities.LasDatabase;
import com.dak.las.utilities.SuperDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListSurveyFragment extends Fragment implements SurveyAdapter.OnSurveyInteractionListener, View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private ArrayList<Survey> surveyArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int identifier=0;
    private LasDatabase db;
    private SurveyAdapter adapter;
    private List<String> idTodelete;
    private View view;
    private FloatingActionButton fabSave;
    private long lastTimClick=0;
    private ChangeFragmentListener changeFragmentListener;
    public static final String PARAM_ID_TRX="id_trx";
    private ImageView imgNotif;

    public ListSurveyFragment() {
        // Required empty public constructor
    }

    public static ListSurveyFragment newInstance() {
        ListSurveyFragment fragment = new ListSurveyFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        db = Room.databaseBuilder(getActivity().getApplicationContext(),
                LasDatabase.class, "db_las").allowMainThreadQueries().build();
        idTodelete = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_list_survey, container, false);
        recyclerView = view.findViewById(R.id.recycleMySurveyOnl);
        swipeRefreshLayout = view.findViewById(R.id.swipeListSurvey);
        fabSave = view.findViewById(R.id.fabSaveMySurveyOnl);
        imgNotif = view.findViewById(R.id.imgListSurvey);
        fabSave.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListSurvey();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        //getDataSurfey();
        getListSurvey();
        return  view;
    }



    private void getListSurvey(){
        swipeRefreshLayout.setRefreshing(true);
        imgNotif.setVisibility(View.GONE);
        final ApiService apiService = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
        final JSONObject paramObject = new JSONObject();
        try{
            paramObject.put(ConstantString.JSON_PARAM_MSISDN,new UserSession().getMsiSdn(getActivity()));
        }catch (Exception e){

        }
        System.out.println("asd JSON REQUEST "+paramObject.toString());
        Call<Survey.ValueSurvey> call = apiService.getAllSurvey(paramObject.toString(),"{}");
        call.enqueue(new Callback<Survey.ValueSurvey>() {
            @Override
            public void onResponse(Call<Survey.ValueSurvey> call, Response<Survey.ValueSurvey> response) {
                try {
                    if (response.body().getRc().equals("00")) {
                        System.out.println("asd response [ListSurvey] "+response.body().getData().toString());
                        surveyArrayList = response.body().getData();
                        adapter = new SurveyAdapter(getActivity(),surveyArrayList,ListSurveyFragment.this,1,"");
                        linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        swipeRefreshLayout.setRefreshing(false);
                    } else if(response.body().getRc().equals("20")) {
                        swipeRefreshLayout.setRefreshing(false);
                        imgNotif.setVisibility(View.VISIBLE);
                    }else if(response.body().getRc().equals("IE")) {
                        swipeRefreshLayout.setRefreshing(false);
                        imgNotif.setImageDrawable(getResources().getDrawable(R.drawable.ic_internal_server_eror));
                        imgNotif.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    swipeRefreshLayout.setRefreshing(false);
                    imgNotif.setImageDrawable(getResources().getDrawable(R.drawable.ic_internal_server_eror));
                    imgNotif.setVisibility(View.VISIBLE);
                }

            }
            @Override
            public void onFailure(Call<Survey.ValueSurvey> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                imgNotif.setImageDrawable(getResources().getDrawable(R.drawable.ic_tidak_ada_koneksi));
                imgNotif.setVisibility(View.VISIBLE);
                Snackbar.make(getActivity().findViewById(R.id.layoutMain),"",Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getListSurvey();
                    }
                }).setActionTextColor(Color.WHITE).show();
                System.out.println("asd [ListSurvey] failure response throwable "+t.getMessage());
                System.out.println("asd [ListSurvey] failure response call "+call.toString());
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list_survey, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       if(id==R.id.action_search){
            Intent inten = new Intent(getActivity(), SearchActivity.class);
            startActivity(inten);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemChecked(String id,int pos,Boolean checked) {
        /**
         * identifier dipake untuk penanda apakah user sudah menceklis atau belum, jika blm menceklis survey manapun maka
         identifier akan bernilai 0(jika 0 maka tidak akan menjalankan method save survey), jika sudah memilih maka akan bernilai lebih dari 1
         */
       if(checked){
           idTodelete.add(String.valueOf(pos));
           identifier++;
           if(identifier<=0){
              fabSave.hide();
           }else{
               fabSave.show();
           }
       }else{
           idTodelete.remove(String.valueOf(pos));
           identifier--;
           if(identifier<=0){
               fabSave.hide();
           }else{
               fabSave.show();
           }
       }
    }

    @Override
    public void btnThreeClicked(String id_survey, String id_trx) {

    }

    @Override
    public void btnTwoClicked(String id_survey, String id_trx) {

    }

    @Override
    public void onViewInventoryClicked(String id_survey) {
        final Intent intent = new Intent(getActivity(), DetailInventoryActivity.class);
        intent.putExtra(PARAM_ID_TRX,id_survey);
        startActivity(intent);
    }

    @Override
    public void onKirimClicked(String id_survey,int pos) {

    }

    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Ambil survey ?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveSurvey();
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.create().show();
    }

    private void saveSurvey(){
        swipeRefreshLayout.setRefreshing(true);
        final ApiService apiService = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
        final JSONObject paramObject = new JSONObject();
        final JSONArray surveys = new JSONArray();
        try{
            paramObject.put(ConstantString.JSON_PARAM_MSISDN,new UserSession().getMsiSdn(getActivity()));
            for(int i=0;i<surveyArrayList.size();i++){
                if(surveyArrayList.get(i).getStatus()!=null && surveyArrayList.get(i).getStatus()){
                    surveys.put(new JSONObject().put("id_survey",surveyArrayList.get(i).getId()));
                }
            }
            paramObject.put("surveys",surveys);
        }catch (Exception e){
            SuperDialog.showDialogMessage(getActivity(),"Error","Terjadi kesalahan pada sistem");
            return;
        }
        System.out.println("asd param save survey "+paramObject.toString());
        Call<String> call = apiService.takeSurvey(paramObject.toString(),"{}");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    System.out.println("asd response save survey "+response.body());
                    JSONObject jsonResponse = new JSONObject(response.body());
                    if(jsonResponse.getString("rc").equals("00")){
                        System.out.println("asd list survey "+surveyArrayList.toString());
                        insertIntoLocalDB(jsonResponse);
                    }else{
                       SuperDialog.showDialogMessage(getActivity(),"Error",jsonResponse.getString("rm"));
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }catch (Exception e){
                    System.out.println("asd [ListSurveyFragment] "+e.getMessage());
                    swipeRefreshLayout.setRefreshing(false);
                    SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_SERVER_RESPONSE_FAILED,Constant.DESC_SERVER_RESPONSE_FAILED);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_CANNOT_CONNECT,Constant.DESC_CANNOT_CONNECT);
                System.out.println("asd [ListSurveyFragment] "+t.getMessage());
            }
        });
    }

    private void insertIntoLocalDB(JSONObject jsonResponse){
        try {
            JSONArray surveys = jsonResponse.getJSONArray("surveys");
            final int sizeSurveys = surveys.length();
            final Form newForm = new Form();
            JSONArray jsonForm;
            JSONObject jsonSurvey;
            int sizejsonForm;

            //proses ini untuk mengambil data yang secara deffault sudah terisi
            for(int i=0;i<sizeSurveys;i++){
                jsonSurvey = surveys.getJSONObject(i);
                jsonForm = jsonSurvey.getJSONArray("form");
                sizejsonForm = jsonForm.length();
                for(int x=0;x<sizejsonForm;x++){
                    newForm.setJsonForm(jsonForm.getJSONObject(x).getJSONArray("element_form").toString());
                    newForm.setSurveyId(jsonSurvey.getInt("id_survey"));
                    newForm.setFormId(jsonForm.getJSONObject(x).getInt("id"));
                    newForm.setMandatory(jsonForm.getJSONObject(x).getBoolean("mandatory"));
                    db.formDao().insertAll(newForm);
                }
            }
            final int size = surveyArrayList.size();
            for(int i=0;i<size;i++){
                if(surveyArrayList.get(i).getStatus()!=null && surveyArrayList.get(i).getStatus()){
                    insertToDB(surveyArrayList.get(i));
                }
            }
            SuperDialog.showListenerDialogMessage(getActivity(), "Success", "Data survey tersimpan ke My Survey", new SuperDialog.OnDialogListener() {
                @Override
                public void onOkClick() {
                    changeFragmentListener.onChangeFragment(2);
                }

                @Override
                public void onCancelClick() {

                }
            });

        }catch (Exception e){
            System.out.println("asd exception[insertLocal] "+e.getMessage());

        }

    }

    private void insertToDB(final Survey survey){
        db.surveyDao().insertAll(survey);
    }

    @Override
    public void onClick(View v) {
        if(SystemClock.elapsedRealtime() < lastTimClick-1000){
            return;
        }
        lastTimClick = SystemClock.elapsedRealtime();
        processClick(v);
    }

    private void processClick(View v) {
       if(v.getId()==R.id.fabSaveMySurveyOnl){
               showDialog();
       }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ChangeFragmentListener) {
            changeFragmentListener = (ChangeFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        changeFragmentListener = null;
    }


}
