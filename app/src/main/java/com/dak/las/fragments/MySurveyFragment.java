package com.dak.las.fragments;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dak.las.R;
import com.dak.las.activities.SendingSurveyActivity;
import com.dak.las.activities.SuperForm;
import com.dak.las.adapters.SurveyAdapter;
import com.dak.las.interfaces.ApiService;
import com.dak.las.models.DynamicView.DynamicView;
import com.dak.las.models.Form.Form;
import com.dak.las.models.Survey.Survey;
import com.dak.las.services.RetrofitService;
import com.dak.las.sessions.DeviceSession;
import com.dak.las.sessions.UserSession;
import com.dak.las.utilities.Constant;
import com.dak.las.utilities.LasDatabase;
import com.dak.las.utilities.SuperDialog;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MySurveyFragment extends Fragment implements SurveyAdapter.OnSurveyInteractionListener{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final int SEND_ACTIVITY_REQUEST_CODE = 123;
    public static final int SENDING_RESULT_CODE=31;
    public static final int SENDING_RESULT_RESPONSE=13;
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    private LasDatabase db;
    private List<Survey> surveys;
    private SurveyAdapter adapter;
    private String account_type;
    private final UserSession userSession = new UserSession();
    private int listPost;
    private ImageView imgNotif;


    public MySurveyFragment() {
        // Required empty public constructor
    }

    public static MySurveyFragment newInstance() {
        MySurveyFragment fragment = new MySurveyFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        db = Room.databaseBuilder(getActivity().getApplicationContext(),
                LasDatabase.class, "db_las").allowMainThreadQueries().build();
        if(userSession.getAccountType(getActivity()).contains("Risk")){
            account_type=userSession.getAccountType(getActivity());
        }else {
            account_type = userSession.getAccountType(getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_survey, container, false);
        recyclerView = v.findViewById(R.id.rvMySurvey);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        imgNotif = v.findViewById(R.id.imgMySurvey);
        getSurvey();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void getSurvey(){
        try {
            surveys = db.surveyDao().getAllSurvey();
            if(!surveys.isEmpty()) {
                if (account_type.contains("Risk")) {
                    adapter = new SurveyAdapter(getActivity(), surveys, MySurveyFragment.this, 4, "");
                } else {
                    adapter = new SurveyAdapter(getActivity(), surveys, MySurveyFragment.this, 2, "");
                }
                recyclerView.setAdapter(adapter);
            }else{
                imgNotif.setImageDrawable(getResources().getDrawable(R.drawable.ic_anda_belum__memiliki_data_survey));
                imgNotif.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            SuperDialog.showDialogMessage(getActivity(),"Failure",e.getMessage());
        }
    }

    @Override
    public void onItemChecked(String id, final int pos,Boolean checked) {
      showDialog(id,pos);
    }

    @Override
    public void btnThreeClicked(final String id_survey,String id_trx) {

        new AsyncTask<Void, String, DynamicView>() {
            @Override
            protected DynamicView doInBackground(Void... voids) {
                if(account_type.contains("Risk")){
                    return db.dynamicViewDao().getId("menu-risk-legal");
                }else {
                    return db.dynamicViewDao().getId("menu-LKA");
                }
            }

            @Override
            protected void onPostExecute(DynamicView dynamicView) {
                super.onPostExecute(dynamicView);
                Intent intent = new Intent(getActivity(),SuperForm.class);
                intent.putExtra(SuperForm.ARG_PARAM1,String.valueOf(dynamicView.getViewId()));
                intent.putExtra(SuperForm.ARG_PARAM2,id_survey);
                startActivity(intent);
            }
        }.execute();

    }

    @Override
    public void btnTwoClicked(final String id_survey,String id_trx) {

        new AsyncTask<Void, String, DynamicView>() {
            @Override
            protected DynamicView doInBackground(Void... voids) {
                    return db.dynamicViewDao().getId("menu-LTC");
            }

            @Override
            protected void onPostExecute(DynamicView dynamicView) {
                super.onPostExecute(dynamicView);
                Intent intent = new Intent(getActivity(),SuperForm.class);
                intent.putExtra(SuperForm.ARG_PARAM1,String.valueOf(dynamicView.getViewId()));
                intent.putExtra(SuperForm.ARG_PARAM2,id_survey);
                startActivity(intent);
            }
        }.execute();
    }

    @Override
    public void onViewInventoryClicked(String id_survey) {

    }

    @Override
    public void onKirimClicked(String id_survey,int pos) {
        System.out.println("asd id_survey "+id_survey);
        listPost = pos;
        showDialogSend(id_survey,pos);
//        final Intent intent = new Intent(getActivity(),SendingSurveyActivity.class);
//        intent.putExtra("id_survey",id_survey);
//        intent.putExtra("pos",pos);
//        startActivityForResult(intent,SEND_ACTIVITY_REQUEST_CODE);

    }

    private void openForm(String id){

    }

    private void hapusSurvey(String id,final int pos){
        try {
            final String id_survey = id;
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    db.surveyDao().delete(id_survey);
                    db.formDao().delete(id_survey);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    deleteFromServer(id_survey,pos);
                }
            }.execute();
        }catch (Exception e){
            System.out.println("asd [mySurveyFragment] "+e.getMessage());
            Toast.makeText(getActivity(),"Gagal menghapus data survey",Toast.LENGTH_SHORT).show();
        }
    }

    private void showDialog(final String id,final int pos){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Hapus data survey ?");
        builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            hapusSurvey(id,pos);
        }
    });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            return;
        }
    });
        builder.create().show();
    }

    private void showDialogSend(final String id_survey,final int pos){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Sistem");
        builder.setTitle("Kirim survey sekarang ?");
        builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getForms(id_survey,pos);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.create().show();
    }


    private void getForms(final String id_survey, final int post){
        new AsyncTask<Void, List<Form>, List<Form>>() {
            @Override
            protected List<Form> doInBackground(Void... voids) {
                return db.formDao().getAllForm(id_survey);
            }

            @Override
            protected void onPostExecute(List<Form> forms) {
                super.onPostExecute(forms);
                //sendSurvey(id_survey,forms,post);
                checkForMandatoryForm(id_survey,forms,post);
            }
        }.execute();
    }

    private void checkForMandatoryForm(final String id_survey,List<Form> forms,final int pos){
        try {
            final List<Mandatory> mandatoryList = new ArrayList<>();
            final JSONArray jsonArray = new JSONArray(new DeviceSession(getActivity()).getListMandatoryForm());
            final int lengthJsonObject = jsonArray.length();
            for(int z=0;z<lengthJsonObject;z++){
                mandatoryList.add(new Mandatory(jsonArray.getJSONObject(z).getInt("id"),jsonArray.getJSONObject(z).getString("form_name"),false));
            }
            final int size = mandatoryList.size();
            final int formLength = forms.size();

            for (int i=0;i<size;i++){
               for(int x=0;x<formLength;x++){
                   if(mandatoryList.get(i).getId()==forms.get(x).getFormId()){
                       mandatoryList.get(i).setFilled(true);
                        break;
                    }
                }
            }

            String form_belum_terisi="";
            for(int k=0;k<mandatoryList.size();k++){
                if(!mandatoryList.get(k).isFilled()){
                    form_belum_terisi=form_belum_terisi+mandatoryList.get(k).getForm_name()+"\n";
                }
            }

            if(form_belum_terisi.length()==0){
                final Intent intent = new Intent(getActivity(),SendingSurveyActivity.class);
                intent.putExtra("id_survey",id_survey);
                intent.putExtra("pos",pos);
                startActivityForResult(intent,SEND_ACTIVITY_REQUEST_CODE);
            }else{
                SuperDialog.showDialogMessage(getActivity(), "Anda belum melengkapi form berikut ini :", form_belum_terisi);
            }
        }catch (Exception e){
            SuperDialog.showDialogMessage(getActivity(),"Failure","Failed to read system data");
        }

    }

    private void sendSurvey(final String id_survey,List<Form> forms,final int pos){
        System.out.println("asd send forms "+forms.toString());
        if(!forms.isEmpty()){
            final SuperDialog progressBar = new SuperDialog(getActivity());
            progressBar.createProgressDialog("Please wait..",getActivity().getLayoutInflater());
            progressBar.show();
            final ApiService apiClient = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
            final JSONObject jsonObject= new JSONObject();
            try{
                jsonObject.put("msisdn",new UserSession().getMsiSdn(getActivity()));
                jsonObject.put("id_trx",id_survey);
                JSONArray data = new JSONArray();
                JSONObject formObject = new JSONObject();
                final int size = forms.size();
                for(int i=0;i<size;i++){
                    formObject = new JSONObject();
                    formObject.put("id_form",forms.get(i).getFormId());
                    formObject.put("form",new JSONArray(forms.get(i).getJsonForm()));
                    data.put(formObject);
                }
                jsonObject.put("data",data);
            }catch (Exception e){
                System.out.println("asd send catch json "+e.getMessage());
            }

            System.out.println("asd send result "+jsonObject.toString());
            Call<String> call = apiClient.sendForm(jsonObject.toString(),"{}");
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    try{
                        final JSONObject jsonRespon = new JSONObject(response.body());
                        if(jsonRespon.getString("rc").equals("00")){
                            progressBar.dismis();
                            SuperDialog.showDialogMessage(getActivity(),"Sukses","Berhasil mengirim data");
                            db.formDao().delete(id_survey);
                            db.surveyDao().delete(id_survey);
                            surveys.remove(pos);
                            adapter.notifyDataSetChanged();
                        }else{
                            progressBar.dismis();
                            SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_SERVER_RESPONSE_FAILED,jsonObject.getString("rm"));
                        }

                    }catch (Exception e){
                        System.out.println("asd [catch MySurvey] "+e.getMessage());
                        progressBar.dismis();
                        SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_SERVER_RESPONSE_FAILED,Constant.DESC_SERVER_RESPONSE_FAILED);
                    }


                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    System.out.println("asd [failure MySurvey] "+t.getMessage());
                    progressBar.dismis();
                    SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_CANNOT_CONNECT,Constant.DESC_CANNOT_CONNECT);

                }
            });


        //Else jika data form kosong
        }else{
            SuperDialog.showDialogMessage(getActivity(),"Error","Anda belum mengisi survey");
        }
    }

    private void deleteFromServer(String id_survey,final int pos){
        final SuperDialog progressBar = new SuperDialog(getActivity());
        progressBar.createProgressDialog("Please wait..",getActivity().getLayoutInflater());
        progressBar.show();
        final ApiService apiClient = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
        final JSONObject jsonObject= new JSONObject();
        try{
            jsonObject.put("msisdn",new UserSession().getMsiSdn(getActivity()));
            jsonObject.put("id_survey",id_survey);
        }catch (Exception e){

        }
        Call<String> call = apiClient.cancelSurvey(jsonObject.toString(),"{}");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                   final JSONObject jsonRespon = new JSONObject(response.body());
                   if(jsonRespon.getString("rc").equals("00")){
                       progressBar.dismis();
                       SuperDialog.showDialogMessage(getActivity(),"Sukses","Berhasil menghapus data");
                       surveys.remove(pos);
                       adapter.notifyDataSetChanged();
                   }else{
                     progressBar.dismis();
                     SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_SERVER_RESPONSE_FAILED,jsonObject.getString("rm"));
                   }

                }catch (Exception e){
                    System.out.println("asd [catch MySurvey] "+e.getMessage());
                    progressBar.dismis();
                    SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_SERVER_RESPONSE_FAILED,Constant.DESC_SERVER_RESPONSE_FAILED);
                }


            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd [failure MySurvey] "+t.getMessage());
                progressBar.dismis();
                SuperDialog.showDialogMessage(getActivity(),Constant.TITLE_CANNOT_CONNECT,Constant.DESC_CANNOT_CONNECT);

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("asd resume ");
    }

    //-----------------OBJECT-------------------
    class Mandatory{
        private int id;
        private String form_name;
        private boolean filled;

        public Mandatory(int id, String form_name, boolean filled) {
            this.id = id;
            this.form_name = form_name;
            this.filled = filled;
        }

        public int getId() {
            return id;
        }

        public String getForm_name() {
            return form_name;
        }

        public boolean isFilled() {
            return filled;
        }

        public void setFilled(boolean filled) {
            this.filled = filled;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==SEND_ACTIVITY_REQUEST_CODE) {
            if (resultCode == SENDING_RESULT_CODE) {
                Toast.makeText(getActivity(), "Survey terkirim", Toast.LENGTH_SHORT).show();
                surveys.remove(listPost);
                adapter.notifyDataSetChanged();
            }else{
                SuperDialog.showDialogMessage(getActivity(),"Failed",data.getStringExtra("result_message"));
            }
        }
    }

}
