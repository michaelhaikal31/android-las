package com.dak.las.fragments

import android.app.AlertDialog
import android.arch.persistence.room.Room
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.opengl.Visibility
import android.os.Bundle
import android.os.SystemClock
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.dak.las.R
import com.dak.las.activities.DetailInventoryActivity
import com.dak.las.adapters.SurveyAdapter
import com.dak.las.interfaces.ApiService
import com.dak.las.interfaces.ChangeFragmentListener
import com.dak.las.models.Form.Form
import com.dak.las.models.Survey.Survey
import com.dak.las.services.RetrofitService
import com.dak.las.sessions.UserSession
import com.dak.las.utilities.Constant
import com.dak.las.utilities.ConstantString
import com.dak.las.utilities.LasDatabase
import com.dak.las.utilities.SuperDialog
import kotlinx.android.synthetic.main.fragment_my_survey_onl.*
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private var surveyArrayList = ArrayList<Survey>()
private var recyclerView: RecyclerView? = null
private var linearLayoutManager: LinearLayoutManager? = null
private var swipeRefreshLayout: SwipeRefreshLayout? = null
private var identifier = 0
private var db: LasDatabase? = null
private var adapter: SurveyAdapter? = null
private var idTodelete: MutableList<String>? = null
private var view: View? = null
private var fabSave: FloatingActionButton? = null
private var lastTimClick: Long = 0
private var changeFragmentListener: ChangeFragmentListener? = null
val PARAM_ID_TRX = "id_trx"
private lateinit var imgNotif : ImageView

class MySurveyOnlFragment : Fragment(),SurveyAdapter.OnSurveyInteractionListener,View.OnClickListener {



    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var swifeRefresh : SwipeRefreshLayout
    private lateinit var recylerView : RecyclerView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        db = Room.databaseBuilder(activity!!.applicationContext,
                LasDatabase::class.java, "db_las").allowMainThreadQueries().build()
        idTodelete = ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v : View =  inflater.inflate(R.layout.fragment_my_survey_onl, container, false)
        fabSave = v.findViewById(R.id.fabSaveMySurveyOnl)
        fabSave?.setOnClickListener(this)
        swifeRefresh = v.findViewById(R.id.swipeMySurveyOnl)
        recylerView = v.findViewById(R.id.recycleMySurveyOnl)
        imgNotif = v.findViewById(R.id.imgMySurveyOnl)

        swifeRefresh.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener { getListSurvey() })
        recylerView.setLayoutManager(LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false))
        //getDataSurfey();
        getListSurvey()
        return v
    }

    override fun onResume() {
        super.onResume()
        println("asd resume ONL")
    }

    override fun onStart() {
        super.onStart()
        println("asd start")
    }

    private fun getListSurvey() {
        surveyArrayList.clear()
        adapter?.notifyDataSetChanged()

        swifeRefresh.setRefreshing(true)
        imgNotif.visibility = View.GONE
        val apiService = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService::class.java)
        val paramObject = JSONObject()
        try {
            paramObject.put(ConstantString.JSON_PARAM_MSISDN, UserSession().getMsiSdn(activity))
            paramObject.put(ConstantString.JSON_REQUEST_GET_SURVEY,0)
        } catch (e: Exception) {

        }
        println("asd JSON REQUEST $paramObject")
        val call = apiService.getAllMySurvey(paramObject.toString(), "{}")
        call.enqueue(object : Callback<Survey.ValueSurvey> {
            override fun onResponse(call: Call<Survey.ValueSurvey>, response: Response<Survey.ValueSurvey>) {
                try {
                    if (response.body()!!.rc == "00") {
                        println("asd response [ListSurvey] " + response.body()!!.data.toString())
                        surveyArrayList = response.body()!!.data
                        adapter = SurveyAdapter(activity, surveyArrayList,this@MySurveyOnlFragment, 1, "")
                        linearLayoutManager = LinearLayoutManager(activity)
                        linearLayoutManager?.setOrientation(LinearLayoutManager.VERTICAL)
                        recylerView.setAdapter(adapter)
                        recyclerView?.setLayoutManager(linearLayoutManager)
                        swifeRefresh.isRefreshing=false
                    } else if (response.body()!!.rc == "20") {
                        swifeRefresh.isRefreshing=false
                        imgNotif.setImageDrawable(resources.getDrawable(R.drawable.ic_data_not_found))
                        imgNotif.visibility = View.VISIBLE
                    } else if (response.body()!!.rc == "IE") {
                        swifeRefresh.isRefreshing=false
                        imgNotif.setImageDrawable(resources.getDrawable(R.drawable.ic_internal_server_eror))
                        imgNotif.visibility = View.VISIBLE
                    }
                } catch (e : Exception) {
                    swifeRefresh.isRefreshing=false
                    imgNotif.setImageDrawable(resources.getDrawable(R.drawable.ic_internal_server_eror))
                    imgNotif.visibility = View.VISIBLE
                }

            }
            override fun onFailure(call: Call<Survey.ValueSurvey>, t: Throwable) {
                swifeRefresh.isRefreshing=false
                Snackbar.make(activity!!.findViewById(R.id.layoutMain), "", Snackbar.LENGTH_INDEFINITE).setAction("Retry") { getListSurvey() }.setActionTextColor(Color.WHITE).show()
                println("asd [ListSurvey] failure response throwable " + t.message)
                println("asd [ListSurvey] failure response call $call")
            }
        })
    }


    override fun btnThreeClicked(id_survey: String?, id_trx: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun btnTwoClicked(id_survey: String?, id_trx: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onViewInventoryClicked(id_survey: String?) {
        val intent = Intent(activity, DetailInventoryActivity::class.java)
        intent.putExtra(PARAM_ID_TRX, id_survey)
        startActivity(intent)
    }

    override fun onKirimClicked(id_survey: String?, pos: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemChecked(id: String?, pos: Int, checked: Boolean) {
        if (checked) {
            idTodelete?.add(pos.toString())
            identifier++
            if (identifier <= 0) {
                fabSave?.hide()
            } else {
                fabSave?.show()
            }
        } else {
            idTodelete?.remove(pos.toString())
            identifier--
            if (identifier <= 0) {
                fabSave?.hide()
            } else {
                fabSave?.show()
            }
        }

    }

    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() < lastTimClick - 1000) {
            return
        }
        lastTimClick = SystemClock.elapsedRealtime()
        processClick(v)
    }

    private fun processClick(v: View) {
        if (v.id == R.id.fabSaveMySurveyOnl) {
            showDialog()
        }
    }

    private fun showDialog() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Ambil survey ?")
        builder.setPositiveButton("Ya") { dialog, which -> saveSurvey() }
        builder.setNegativeButton("Batal", DialogInterface.OnClickListener { dialog, which -> return@OnClickListener })
        builder.create().show()
    }

    private fun saveSurvey() {
        swifeRefresh?.setRefreshing(true)
        val apiService = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService::class.java)
        val paramObject = JSONObject()
        val surveys = JSONArray()
        try {
            paramObject.put(ConstantString.JSON_PARAM_MSISDN, UserSession().getMsiSdn(activity))
            for (i in surveyArrayList.indices) {
                if (surveyArrayList[i].status != null && surveyArrayList[i].status!!) {
                    surveys.put(JSONObject().put("id_survey", surveyArrayList[i].id))
                }
            }
            paramObject.put("surveys", surveys)
        } catch (e: Exception) {
            SuperDialog.showDialogMessage(activity, "Error", "Terjadi kesalahan pada sistem")
            return
        }

        println("asd param $paramObject")
        val call = apiService.takeSurvey(paramObject.toString(), "{}")
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    val jsonResponse = JSONObject(response.body())
                    if (jsonResponse.getString("rc").equals("00")) {
                        println("asd list survey $surveyArrayList")
                        insertIntoLocalDB(jsonResponse)
                    }else{
                        imgNotif.setImageDrawable(resources.getDrawable(R.drawable.ic_internal_server_eror))
                        imgNotif.visibility = View.VISIBLE
                    }
                    swifeRefresh.isRefreshing=false
                } catch (e: Exception) {
                    println("asd [ListSurveyFragment] " + e.message)
                    imgNotif.setImageDrawable(resources.getDrawable(R.drawable.ic_internal_server_eror))
                    imgNotif.visibility = View.VISIBLE
                    swifeRefresh.isRefreshing=false
                    SuperDialog.showDialogMessage(activity, Constant.TITLE_SERVER_RESPONSE_FAILED, Constant.DESC_SERVER_RESPONSE_FAILED)
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                imgNotif.visibility = View.VISIBLE
                swifeRefresh.setRefreshing(false)
                SuperDialog.showDialogMessage(activity, Constant.TITLE_CANNOT_CONNECT, Constant.DESC_CANNOT_CONNECT)
                println("asd [ListSurveyFragment] " + t.message)
            }
        })
    }

    private fun insertIntoLocalDB(jsonResponse: JSONObject) {
        try {
            val surveys = jsonResponse.getJSONArray("surveys")
            val sizeSurveys = surveys.length()
            val newForm = Form()
            var jsonForm: JSONArray
            var jsonSurvey: JSONObject
            var sizejsonForm: Int
            for (i in 0 until sizeSurveys) {
                jsonSurvey = surveys.getJSONObject(i)
                jsonForm = jsonSurvey.getJSONArray("form")
                sizejsonForm = jsonForm.length()
                for (x in 0 until sizejsonForm) {
                    newForm.jsonForm = jsonForm.getJSONObject(x).getJSONArray("element_form").toString()
                    newForm.surveyId = jsonSurvey.getInt("id_survey")
                    newForm.formId = jsonForm.getJSONObject(x).getInt("id")
                    newForm.mandatory = jsonForm.getJSONObject(x).getBoolean("mandatory")
                    db?.formDao()?.insertAll(newForm)
                }


            }

            val size = surveyArrayList.size
            for (i in 0 until size) {
                if (surveyArrayList[i].status != null && surveyArrayList[i].status!!) {
                    insertToDB(surveyArrayList[i])
                }
            }
            SuperDialog.showListenerDialogMessage(activity, "Success", "Data survey tersimpan ke My Survey", object : SuperDialog.OnDialogListener {
                override fun onOkClick() {
                    getListSurvey()
                }
                override fun onCancelClick() {

                }
            })

        } catch (e: Exception) {
            println("asd exception[insertLocal] " + e.message)

        }

    }

    private fun insertToDB(survey: Survey) {
        db?.surveyDao()?.insertAll(survey)
    }

    override fun onDestroy() {
        super.onDestroy()
        db?.close()
    }

}
