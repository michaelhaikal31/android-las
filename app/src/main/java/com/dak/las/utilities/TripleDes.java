package com.dak.las.utilities;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

/**
 * Created by acs on 9/8/17.
 */

public class TripleDes {
    // public final static String SECRET_KEY = "0102030405060708090A0B0C0D0E0F";
    //public final static String SECRET_KEY = "0908070605040302010A0B0C0D0E0F";


    private DESedeKeySpec desKeySpec;
    private IvParameterSpec ivSpec;

    public TripleDes(String stringKey) throws Exception {
        try {
            byte[] keyBytes = stringKey.getBytes();
            this.desKeySpec = new DESedeKeySpec(keyBytes);
            this.ivSpec = new IvParameterSpec(stringKey.substring(0, 8).getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  String encrypt(String unencryptedString) {
        try {
            byte[] ori = unencryptedString.getBytes();
            SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
            SecretKey key = factory.generateSecret(this.desKeySpec);
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key, this.ivSpec);
            byte[] crypted = cipher.doFinal(ori);
            return new String(Base64.encodeToString(crypted, Base64.NO_WRAP));
        }  catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public String decrypt(String crypted) {
        try {
            byte[] crypt = Base64.decode(crypted.getBytes(), Base64.NO_WRAP);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("DESede");
            SecretKey key = factory.generateSecret(this.desKeySpec);
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, key, this.ivSpec);
//			return cipher.doFinal(crypted);
            return new String(cipher.doFinal(crypt));
        }  catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
