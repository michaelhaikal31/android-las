package com.dak.las.utilities

import okhttp3.Interceptor
import okhttp3.Response

class SupportInterceptor: Interceptor {

    /**
     * Interceptor class for setting of the headers for every request
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request?.newBuilder()
                ?.addHeader("Content-Type", "multipart/form-data")
                ?.addHeader("x-api-key", Constant.getKey(24))
                ?.build()
        return chain.proceed(request)
    }
}