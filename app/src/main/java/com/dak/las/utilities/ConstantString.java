package com.dak.las.utilities;

public class ConstantString {
    public static String EMPTY_FORM="Silahkan lengkapi data";
    public static String PHONE_NUMBER_EMPTY = "Silahkan isi nomor telepon anda !";
    public static String PHONE_NUMBER_LENGTH = "Panjang minimal nomor telepon 12 digit";
    public static String JSON_PARAM_MSISDN = "msisdn";
    public static String JSON_REQUEST_GET_SURVEY="request";
}
