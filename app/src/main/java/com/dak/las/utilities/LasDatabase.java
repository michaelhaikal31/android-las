package com.dak.las.utilities;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.dak.las.models.DynamicView.DynamicView;
import com.dak.las.models.DynamicView.DynamicViewDao;
import com.dak.las.models.Form.Form;
import com.dak.las.models.Form.FormDao;
import com.dak.las.models.Survey.Survey;
import com.dak.las.models.Survey.SurveyDao;


@Database(entities = {DynamicView.class,Survey.class,Form.class}, version = 1)
public abstract class LasDatabase extends RoomDatabase {
    public abstract DynamicViewDao dynamicViewDao();
    public abstract SurveyDao surveyDao();
    public abstract FormDao formDao();
}

