package com.dak.las.utilities;

public class Constant {
    public static String KEY_DES_PIN = "L4SD1G1T4L4M0R3KR1Y4N3S14";
    public static String SERVICE_HOST="http://117.54.3.201:9967";
    public static String SERVICE_PATH = "/crylas/ws_dev/";
    public static String SERVICE_URL = SERVICE_HOST+SERVICE_PATH;
    public static String SERVICE_BASE = "http://117.54.3.201:9967/crylas/ws_dev/";
    public static final int TIMOUT=20;

    public static final String TITLE_CANNOT_CONNECT="Error";
    public static final String DESC_CANNOT_CONNECT="Gagal terhubung ke server, periksa koneksi anda !";
    public static final String TITLE_SERVER_RESPONSE_FAILED="Error";
    public static final String DESC_SERVER_RESPONSE_FAILED="Gagal membaca respon server, silahkan coba beberapa saat lagi";

    private static final String ALPHA_NUMERIC_STRING = "ABCDEF0123456789";
    public static String getKey(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

}
