package com.dak.las.utilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.dak.las.R;

import org.w3c.dom.Text;

public class SuperDialog {
    Context context;
    AlertDialog dialog;

    public SuperDialog(Context context) {
        this.context = context;
    }

    public static void showDialogMessage(Context context,String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.show();
    }

    public static void showListenerDialogMessage(Context context,String title,String message,final OnDialogListener listener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onOkClick();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onCancelClick();
            }
        });
        builder.show();
    }

    public static void showOkListenerDialogMessage(Context context,String title,String message,final OnDialogListener listener){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onOkClick();
                listener.onCancelClick();
            }
        });

        builder.show();
    }

    public void createProgressDialog(String title,LayoutInflater inflater){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = inflater.inflate(R.layout.dialog_progressbar,null);
        builder.setView(view);
        TextView textView = view.findViewById(R.id.txtProgresDialog);
        textView.setText(title);
        builder.create();
        dialog = builder.create();
    }

    public void show(){
        dialog.show();
    }

    public void dismis(){
        dialog.dismiss();
    }

    public interface OnDialogListener{
        void onOkClick();
        void onCancelClick();
    }





}
