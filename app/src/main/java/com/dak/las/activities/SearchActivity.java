package com.dak.las.activities;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.dak.las.R;
import com.dak.las.adapters.SurveyAdapter;
import com.dak.las.interfaces.ApiService;
import com.dak.las.models.SearchSurveySuggestion;
import com.dak.las.models.Survey.Survey;
import com.dak.las.services.RetrofitService;
import com.dak.las.sessions.UserSession;
import com.dak.las.utilities.Constant;
import com.dak.las.utilities.ConstantString;
import com.dak.las.utilities.LasDatabase;
import com.dak.las.utilities.SuperDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity implements SurveyAdapter.OnSurveyInteractionListener, View.OnClickListener {

    private FloatingSearchView searchView;
    private ApiService apiService;
    private Call<SearchSurveySuggestion.ValueSearchSurvey> call;
    private RecyclerView recyclerView;
    private List<Survey> surveyArrayList;
    private SurveyAdapter adapter;
    private FloatingActionButton fabSave;
    private LasDatabase db;
    private long mLastClick = 0;
    private ArrayList<String> idTodelete;
    private int identifier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchView = findViewById(R.id.floating_search_view);
        recyclerView = findViewById(R.id.rvSearch);
        fabSave = findViewById(R.id.fabSaveMySurveyOnl);
        fabSave.setOnClickListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        searchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {

                if (!oldQuery.equals("") && newQuery.equals("")) {
                    searchView.clearSuggestions();
                } else {

                    //this shows the top left circular progress
                    //you can call it where ever you want, but
                    //it makes sense to do it when loading something in
                    //the background.
                    //searchView.showProgress();

                    //simulates a query call to a data source
                    //with a new query.
//                    DataHelper.findSuggestions(getActivity(), newQuery, 5,
//                            FIND_SUGGESTION_SIMULATED_DELAY, new DataHelper.OnFindSuggestionsListener() {
//
//                                @Override
//                                public void onResults(List<ColorSuggestion> results) {
//
//                                    //this will swap the data and
//                                    //render the collapse/expand animations as necessary
//                                    mSearchView.swapSuggestions(results);
//
//                                    //let the users know that the background
//                                    //process has completed
//                                    mSearchView.hideProgress();
//                                }
//                            });


                    System.out.println("asd onSearchTextChanged()");
                }

            }
        });
        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
                searchView.showProgress();
                getListSurvey(currentQuery);
            }
        });
        searchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
            @Override
            public void onHomeClicked() {
                finish();
            }
        });
        db = Room.databaseBuilder(this.getApplicationContext(),
                LasDatabase.class, "db_las").allowMainThreadQueries().build();
        idTodelete = new ArrayList<>();
    }

    private void getListSurvey(final String keyword){
        JSONObject paramObject = new JSONObject();
        try{
            paramObject.put(ConstantString.JSON_PARAM_MSISDN,new UserSession().getMsiSdn(this));
            paramObject.put("param",keyword);
        }catch (Exception e){

        }
        System.out.println("asd JSON REQUEST "+paramObject.toString());
        apiService = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
        Call<Survey.ValueSurvey> call = apiService.search(paramObject.toString(),"{}");
        call.enqueue(new Callback<Survey.ValueSurvey>() {
            @Override
            public void onResponse(Call<Survey.ValueSurvey> call, Response<Survey.ValueSurvey> response) {
                try {
                    if (response.body().getRc().equals("00")) {
                        surveyArrayList = response.body().getData();
                        adapter = new SurveyAdapter(SearchActivity.this,surveyArrayList, SearchActivity.this,1,"");
                        recyclerView.setAdapter(adapter);
                        searchView.hideProgress();

                    } else {
                        searchView.hideProgress();
                        SuperDialog.showDialogMessage(SearchActivity.this, "Failed", response.body().getRm());
                    }

                }catch (Exception e){
                    System.out.println("asd [listSurvey] " + e.getMessage());
                    searchView.hideProgress();
                    SuperDialog.showDialogMessage(SearchActivity.this,"Failure","Internal server error");
                }

            }
            @Override
            public void onFailure(Call<Survey.ValueSurvey> call, Throwable t) {
               searchView.hideProgress();
                Snackbar.make(findViewById(R.id.rootLayoutSearch),"Cannot connect to server",Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getListSurvey(keyword);
                    }
                }).setActionTextColor(Color.WHITE).show();
                System.out.println("asd [ListSurvey] failure response throwable "+t.getMessage());
                System.out.println("asd [ListSurvey] failure response call "+call.toString());
            }
        });
    }


    @Override
    public void onItemChecked(String id, int pos, Boolean checked) {
        if(checked){
            idTodelete.add(String.valueOf(pos));
            identifier++;
            if(identifier<=0){
                fabSave.hide();
            }else{
                fabSave.show();
            }
        }else{
            idTodelete.remove(String.valueOf(pos));
            identifier--;
            if(identifier<=0){
                fabSave.hide();
            }else{
                fabSave.show();
            }
        }
    }

    @Override
    public void btnThreeClicked(String id_survey, String id_trx) {

    }

    @Override
    public void btnTwoClicked(String id_survey, String id_trx) {

    }

    @Override
    public void onViewInventoryClicked(String id_survey) {
        final Intent intent = new Intent(this, DetailInventoryActivity.class);
        startActivity(intent);
    }

    @Override
    public void onKirimClicked(String id_survey, int pos) {

    }

    @Override
    public void onClick(View v) {
        if(SystemClock.elapsedRealtime()<mLastClick-1000){
            return;
        }
        mLastClick = SystemClock.elapsedRealtime();
        processClickEvent(v);
    }

    private void processClickEvent(View v) {
        if(v.getId()==R.id.fabSaveMySurveyOnl){
            showDialog();
        }
    }

    private void saveSurvey(){
        searchView.showProgress();
        final ApiService apiService = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
        final JSONObject paramObject = new JSONObject();
        final JSONArray surveys = new JSONArray();
        try{
            paramObject.put(ConstantString.JSON_PARAM_MSISDN,new UserSession().getMsiSdn(this));
            for(int i=0;i<surveyArrayList.size();i++){
                if(surveyArrayList.get(i).getStatus()!=null && surveyArrayList.get(i).getStatus()){
                    surveys.put(new JSONObject().put("id_survey",surveyArrayList.get(i).getId()));
                }
            }
            paramObject.put("surveys",surveys);
        }catch (Exception e){
            SuperDialog.showDialogMessage(this,"Error","Terjadi kesalahan pada sistem");
            return;
        }
        System.out.println("asd param "+paramObject.toString());
        Call<String> call = apiService.takeSurvey(paramObject.toString(),"{}");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    final JSONObject jsonObject = new JSONObject(response.body());
                    if(jsonObject.getString("rc").equalsIgnoreCase("00")){
                        final int size = surveyArrayList.size();
                        for(int i=0;i<size;i++){
                            if(surveyArrayList.get(i).getStatus()!=null && surveyArrayList.get(i).getStatus()){
                                insertToDB(surveyArrayList.get(i));
                            }
                        }
                        final int length = idTodelete.size();

                        System.out.println("asd list survey "+surveyArrayList.toString());
                        SuperDialog.showDialogMessage(SearchActivity.this,"Success","Data survey tersimpan");
                        adapter.notifyDataSetChanged();


                    }else{
                        SuperDialog.showDialogMessage(SearchActivity.this,"Error",jsonObject.getString("rm"));
                    }
                    searchView.hideProgress();
                }catch (Exception e){
                    System.out.println("asd [ListSurveyFragment] "+e.getMessage());
                   searchView.hideProgress();
                    SuperDialog.showDialogMessage(SearchActivity.this,Constant.TITLE_SERVER_RESPONSE_FAILED,Constant.DESC_SERVER_RESPONSE_FAILED);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                searchView.hideProgress();
                SuperDialog.showDialogMessage(SearchActivity.this,Constant.TITLE_CANNOT_CONNECT,Constant.DESC_CANNOT_CONNECT);
                System.out.println("asd [ListSurveyFragment] "+t.getMessage());
            }
        });
    }

    private void insertToDB(final Survey survey){
        db.surveyDao().insertAll(survey);
    }

    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ambil survey ?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveSurvey();
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.create().show();
    }

}
