package com.dak.las.activities

import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import com.dak.las.R
import com.dak.las.interfaces.ApiService
import com.dak.las.models.DynamicView.DynamicView
import com.dak.las.models.Form.Form
import com.dak.las.models.Survey.Survey
import com.dak.las.services.RetrofitService
import com.dak.las.sessions.DeviceSession
import com.dak.las.sessions.UserSession
import com.dak.las.utilities.Constant
import com.dak.las.utilities.LasDatabase
import com.dak.las.utilities.SuperDialog
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InitializingActivity : AppCompatActivity(){

    private val deviceSession  = DeviceSession(this@InitializingActivity)
    private var db : LasDatabase? = null
    private var jsonObject: JSONObject? = null
    private var arrayMenu: JSONArray? = null
    private var progressBar: ProgressBar? = null
    private var textView: TextView?=null
    private var lottieAnimationView: LottieAnimationView?=null
    private var arrayMandatory=JSONArray()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initializing)
        db = Room.databaseBuilder(this@InitializingActivity,LasDatabase::class.java,"db_las").allowMainThreadQueries().build()
        if(!deviceSession.isInitialized){
            initialize()
        }else{
            startActivity(Intent(this@InitializingActivity,MainActivity::class.java))
            finish()
        }
    }

    fun loadMenu(){
        try{
            arrayMenu = jsonObject?.getJSONArray("menu")
            var dynamicView : DynamicView
            var menuObject : JSONObject
            for(i in 0..(arrayMenu!!.length()-1)){
                dynamicView = DynamicView()
                menuObject = arrayMenu!!.getJSONObject(i)
                dynamicView.viewId = menuObject.getInt("id")
                dynamicView.viewTitle = menuObject.getString("view_title")
                dynamicView.jenis = "menu-" + menuObject.getString("tipe")
                dynamicView.from = menuObject.getInt("from").toString()
                if (menuObject.getBoolean("main")) {
                    deviceSession.setMainActivity(menuObject.getInt("id"))
                }
                dynamicView.jsonView = menuObject.getJSONArray("list_menu").toString()
                db?.dynamicViewDao()?.insertAll(dynamicView)
            }
            loadForm()

        }catch (e : Exception){
            SuperDialog.showOkListenerDialogMessage(this, Constant.TITLE_SERVER_RESPONSE_FAILED, Constant.DESC_SERVER_RESPONSE_FAILED,object:SuperDialog.OnDialogListener{
                override fun onOkClick() {
                    finish()
                }

                override fun onCancelClick() {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })
            println("asd error load menu "+e.message)
        }
    }

    fun loadForm(){
        try{
            arrayMenu = jsonObject?.getJSONArray("form")
            var dynamicView: DynamicView
            var menuObject: JSONObject
            for(i in 0..(arrayMenu!!.length()-1)){
                dynamicView = DynamicView()
                menuObject = arrayMenu!!.getJSONObject(i)
                dynamicView.viewId = menuObject.getInt("id")
                dynamicView.viewTitle = menuObject.getString("view_title")
                dynamicView.jenis = "form"
                dynamicView.url = menuObject.getString("url")
                dynamicView.to = menuObject.getInt("to").toString()
                dynamicView.from = menuObject.getInt("from").toString()
                dynamicView.jsonView = menuObject.getJSONArray("element_form").toString()
                dynamicView.mandatory = menuObject.getBoolean("mandatory")
                if(dynamicView.mandatory){
                    println("asd init mandatory "+dynamicView.viewId+"|"+dynamicView.viewTitle)
                    arrayMandatory.put(JSONObject().put("id",dynamicView.viewId).put("form_name",dynamicView.viewTitle))
                }
                db?.dynamicViewDao()?.insertAll(dynamicView)
            }
            deviceSession.isInitialized = true
            lottieAnimationView?.setVisibility(View.INVISIBLE)
            deviceSession.setListMandatoryForms(arrayMandatory.toString())
//            startActivity(Intent(this@InitializingActivity,HomeActivity::class.java))
//            finish()
            loadTakedSurvey()
        }catch (e:Exception){
            println("asd error load form "+e.message)
            SuperDialog.showOkListenerDialogMessage(this, Constant.TITLE_SERVER_RESPONSE_FAILED, Constant.DESC_SERVER_RESPONSE_FAILED,object:SuperDialog.OnDialogListener{
                override fun onOkClick() {
                    finish()
                }
                override fun onCancelClick() {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            })
        }
    }

    private fun loadTakedSurvey() {
        try {
            val surveys = jsonObject?.getJSONArray("survey_data")
            val sizeSurveys = surveys?.length()
            val newForm = Form()
            var jsonForm: JSONArray
            var jsonSurvey: JSONObject
            var sizejsonForm: Int

            //proses ini untuk mengambil data yang secara deffault sudah terisi
            for (i in 0 until sizeSurveys!!) {
                jsonSurvey = surveys.getJSONObject(i)
                jsonForm = jsonSurvey.getJSONArray("form")
                sizejsonForm = jsonForm.length()
                for (x in 0 until sizejsonForm) {
                    newForm.jsonForm = jsonForm.getJSONObject(x).getJSONArray("element_form").toString()
                    newForm.surveyId = jsonSurvey.getInt("id_survey")
                    newForm.formId = jsonForm.getJSONObject(x).getInt("id")
                    newForm.mandatory = jsonForm.getJSONObject(x).getBoolean("mandatory")
                    db?.formDao()?.insertAll(newForm)
                }
            }

            val taked_survey = jsonObject?.getJSONArray("taked_survey")
            val array_size = taked_survey?.length()
            var surveyArrayList = ArrayList<Survey>()
            var objectSurvey : JSONObject
            for(i in 0 until array_size!!){
                objectSurvey = taked_survey.getJSONObject(i)
                surveyArrayList.add(Survey(objectSurvey.getInt("id").toString(),objectSurvey.getString("nama"),objectSurvey.getString("flapon"),objectSurvey.getString("produk"),objectSurvey.getString("alamat"),objectSurvey.getString("cabang")))
            }
            val size = surveyArrayList.size
            for (i in 0 until size) {
                insertToDB(surveyArrayList[i])
            }
            startActivity(Intent(this@InitializingActivity,HomeActivity::class.java))
            finish()
        } catch (e: Exception) {
            println("asd error load taked survey "+e.message)
            SuperDialog.showOkListenerDialogMessage(this, Constant.TITLE_SERVER_RESPONSE_FAILED, Constant.DESC_SERVER_RESPONSE_FAILED,object:SuperDialog.OnDialogListener{
                override fun onOkClick() {
                    finish()
                }
                override fun onCancelClick() {

                }
            })
        }

    }

    fun insertToDB(survey: Survey) {
        println("asd insert id "+survey.id)
        db?.surveyDao()?.insertAll(survey)
    }

    fun initialize(){
        val jsonParam = JSONObject()
        try{
            jsonParam.put("msisdn", UserSession().getMsiSdn(this))
        }catch (e : Exception){
            lottieAnimationView?.setVisibility(View.INVISIBLE)
            val intent = Intent(this@InitializingActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
            return
        }

        if (UserSession().getMsiSdn(this) == "") {
            val intent = Intent(this@InitializingActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        val apiService = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService::class.java)
        val call = apiService.initialize(jsonParam.toString(), "{}")
        call.enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    val jsonRespomse = JSONObject(response.body())
                    if (jsonRespomse.getString("rc").equals("00")) {
                        jsonObject = jsonRespomse.getJSONObject("data")
                        loadMenu()
                    } else {
                        resultDialog(Constant.TITLE_SERVER_RESPONSE_FAILED, jsonRespomse.getString("rm"))
                    }
                } catch (e: Exception) {
                    println("asd [initializing] " + e.message)
                    resultDialog(Constant.TITLE_SERVER_RESPONSE_FAILED, Constant.DESC_SERVER_RESPONSE_FAILED)
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                println("asd [initializingActivity] " + t.message)
                resultDialog(Constant.TITLE_CANNOT_CONNECT, Constant.DESC_CANNOT_CONNECT)
            }
        })
    }

    private fun resultDialog(title: String, message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("Retry") { _, _ -> initialize() }
        builder.setNegativeButton("Keluar") { _, _ -> finish() }
        builder.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        db?.close()
    }
}