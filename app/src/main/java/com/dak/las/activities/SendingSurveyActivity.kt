package com.dak.las.activities

import android.arch.persistence.room.Room
import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.dak.las.interfaces.ApiService
import com.dak.las.models.Form.Form
import com.dak.las.models.Survey.Survey
import com.dak.las.services.RetrofitService
import com.dak.las.sessions.UserSession
import com.dak.las.utilities.Constant
import com.dak.las.utilities.LasDatabase
import com.dak.las.utilities.SuperDialog
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.content.Intent
import android.R
import com.dak.las.fragments.MySurveyFragment


class SendingSurveyActivity : AppCompatActivity() {

    private var db: LasDatabase? = null
    private var pos: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.dak.las.R.layout.activity_sending_survey)
        db = Room.databaseBuilder(this,
                LasDatabase::class.java, "db_las").allowMainThreadQueries().build()
        pos = intent.getIntExtra("pos",0)
        println("asd pos [sending] "+pos)
        getForms(intent.getStringExtra("id_survey"))
    }

    private fun getForms(id_survey: String) {
        object : AsyncTask<Void, List<Form>, List<Form>>() {
            override fun doInBackground(vararg voids: Void): List<Form> {
                return db!!.formDao().getAllForm(id_survey)
            }

            override fun onPostExecute(forms: List<Form>) {
                super.onPostExecute(forms)
                sendSurvey(id_survey, forms)
                //checkForMandatoryForm(id_survey,forms,post);
            }
        }.execute()
    }

    private fun sendSurvey(id_survey: String, forms: List<Form>) {
        if (!forms.isEmpty()) {
            val apiClient = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService::class.java)
            val jsonObject = JSONObject()
            try {

                jsonObject.put("msisdn", UserSession().getMsiSdn(this))
                jsonObject.put("id_trx", id_survey)
                val data = JSONArray()
                var formObject = JSONObject()
                val size = forms.size
                for (i in 0 until size) {
                    formObject = JSONObject()
                    formObject.put("id_form", forms[i].formId)
                    formObject.put("form", JSONArray(forms[i].jsonForm))
                    data.put(formObject)
                }
                jsonObject.put("data", data)
            } catch (e: Exception) {
                println("asd send catch json " + e.message)
            }
            val call = apiClient.sendForm(jsonObject.toString(), "{}")
            call.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    try {
                        val jsonRespon = JSONObject(response.body())
                        println("asd response result [SendingSurvey] "+response.body())
                        if (jsonRespon.getString("rc") == "00") {
                            db!!.formDao().delete(id_survey)
                            db!!.surveyDao().delete(id_survey)
                            val output = Intent()
                            setResult(MySurveyFragment.SENDING_RESULT_CODE, output)
                            finish()
                        } else {
                            val output = Intent()
                            output.putExtra("result_message",jsonRespon.getString("rm"))
                            setResult(MySurveyFragment.SENDING_RESULT_RESPONSE, output)
                            finish()
                        }
                    } catch (e: Exception) {
                        val output = Intent()
                        println("asd exception [SendingSurvey] "+e.message)
                        output.putExtra("result_message",Constant.DESC_SERVER_RESPONSE_FAILED)
                        setResult(MySurveyFragment.SENDING_RESULT_RESPONSE, output)
                        finish()
                    }
                }
                override fun onFailure(call: Call<String>, t: Throwable) {
                    println("asd failure [SendingSurvey] "+t.message)
                    val output = Intent()
                    output.putExtra("result_message",Constant.DESC_CANNOT_CONNECT)
                    setResult(MySurveyFragment.SENDING_RESULT_RESPONSE, output)
                    finish()
                }
            })


            //Else jika data form kosong
        } else {
            SuperDialog.showDialogMessage(this, "Error", "Anda belum mengisi survey")
        }
    }

}
