package com.dak.las.activities

import android.os.Build
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.WindowManager
import com.dak.las.R
import com.dak.las.fragments.MySurveyFragment
import com.dak.las.fragments.MySurveyOnlFragment

class MySurveyActivity : AppCompatActivity() {

    private lateinit var pager : ViewPager
    private lateinit var tabLayout: TabLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        }
        setContentView(R.layout.activity_my_survey)
        val container : ConstraintLayout = findViewById(R.id.containerMySurvey)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        tabLayout = findViewById(R.id.tab_layout)
        tabLayout.addTab(tabLayout.newTab().setText("Offline"))
        tabLayout.addTab(tabLayout.newTab().setText("Online"))
        addfragment(MySurveyFragment(),"Offline ")

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {

                when (tabLayout.selectedTabPosition) {
                    0 -> addfragment(MySurveyFragment(), "Offline")
                    1 -> addfragment(MySurveyOnlFragment(),"Online")
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }

    internal fun addfragment(fragment: Fragment, packageName: String) {
        val fragmentManager = supportFragmentManager
        // this will clear the back stack and displays no animation on the screen
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        val trans = supportFragmentManager
                .beginTransaction()
        trans.add(R.id.frameLayoutMySurvey, fragment, "MAIN")
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        trans.addToBackStack(packageName)
        trans.commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

}
