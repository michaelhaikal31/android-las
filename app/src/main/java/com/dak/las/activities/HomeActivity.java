package com.dak.las.activities;

import android.Manifest;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dak.las.R;
import com.dak.las.sessions.DeviceSession;
import com.dak.las.sessions.UserSession;
import com.dak.las.utilities.LasDatabase;
import com.dak.las.utilities.LocationHelper;
import com.dak.las.utilities.SuperDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int MY_PERMISSIONS_REQUEST_LIST = 121 ;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 123;
    private ImageButton btnListSurvey;
    private ImageButton btnMySurvey;
    private ImageButton btnOnGoing;
    private ImageButton btnLogout;
    private long mTimeLeft=0;
    public static final String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btnListSurvey = findViewById(R.id.btnListSurvey);
        btnMySurvey = findViewById(R.id.btnMySurvey);
        btnOnGoing = findViewById(R.id.btnOnGoing);
        btnLogout = findViewById(R.id.btnLogout);
        btnListSurvey.setOnClickListener(this);
        btnMySurvey.setOnClickListener(this);
        btnOnGoing.setOnClickListener(this);
        btnLogout.setOnClickListener(this);

        //requestPermission();





    }


    @Override
    public void onClick(View v) {
        if(SystemClock.elapsedRealtime()-mTimeLeft<1000){
            return;
        }
        mTimeLeft=SystemClock.elapsedRealtime();
        handleClick(v);
    }

    private void handleClick(View v) {
        Intent intent = new Intent(this,MainActivity.class);
        switch (v.getId()){
            case R.id.btnListSurvey:
                intent.putExtra(MainActivity.MENU,1);
                startActivity(intent);
                break;
            case R.id.btnMySurvey:
                intent = new Intent(this,MySurveyActivity.class);
                startActivity(intent);
                break;
            case R.id.btnOnGoing:
                intent.putExtra(MainActivity.MENU,2);
                startActivity(intent);
                break;
            case R.id.btnLogout:

                SuperDialog.showListenerDialogMessage(this, "Logout", "Data survey  anda yang tersimpan di perangkat anda akan terhapus, lanjutkan ?", new SuperDialog.OnDialogListener() {
                    @Override
                    public void onOkClick() {
                        clearAllData();
                    }

                    @Override
                    public void onCancelClick() {
                        return;
                    }
                });
                break;
        }
    }

    private void requestPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                &&  ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                &&  ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                &&  ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions,
                    MY_PERMISSIONS_REQUEST_LIST);
        }else{
            getCurrentLocation();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==MY_PERMISSIONS_REQUEST_LIST){
            getCurrentLocation();
        }
    }

    private void clearAllData() {
        new UserSession().clearSession(HomeActivity.this);
        new DeviceSession(HomeActivity.this).clearSession();
        final LasDatabase db = Room.databaseBuilder(this,
                LasDatabase.class, "db_las").allowMainThreadQueries().build();
        db.formDao().clearFormTable();
        db.dynamicViewDao().clearDynamicViewTable();
        db.surveyDao().clearSurveyTable();
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }

    private void getCurrentLocation(){
        locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LocationHelper locationHelper = new LocationHelper(this, new LocationHelper.LocationHelperInterface() {
                @Override
                public void onLocationReady(String lat, String longi) {
                    Toast.makeText(HomeActivity.this,lat+"|"+longi,Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            SuperDialog.showDialogMessage(this,"Gps non-aktif","Silahkan aktifkan GPS di smartphone anda agar kami dapat melacak lokasi telepon anda !");
        }
    }
}
