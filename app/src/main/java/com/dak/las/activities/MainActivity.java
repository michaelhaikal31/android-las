package com.dak.las.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.dak.las.R;
import com.dak.las.fragments.ListSurveyFragment;
import com.dak.las.fragments.MySurveyFragment;
import com.dak.las.fragments.OnGoingFragment;
import com.dak.las.interfaces.ChangeFragmentListener;
import com.dak.las.sessions.UserSession;

public class MainActivity extends AppCompatActivity
        implements  ChangeFragmentListener {

    private Fragment mFragmentToSet;
    private DrawerLayout drawer;
    private Boolean logout=false;
    private UserSession userSession;
    private TextView toolbar_title;
    private NavigationView navigationView;
    private Toolbar toolbar, searchtollbar;
    public static String MENU="menu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        setContentView(R.layout.activity_main);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar_title = findViewById(R.id.toolbar_title);
        drawer =  findViewById(R.id.drawer_layout);

//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.addDrawerListener(toggle);
//        toggle.syncState();
//
//
//        navigationView =  findViewById(R.id.nav_view);
//        View headerView = navigationView.getHeaderView(0);
//        final TextView navUsername =  headerView.findViewById(R.id.txtNamaDrawer);
//        final TextView navDesc =  headerView.findViewById(R.id.txtKeteranganDrawer);
//        userSession = new UserSession();
//        navUsername.setText(userSession.getName(this));
//        navDesc.setText(userSession.getMsiSdn(this));
//        navigationView.setNavigationItemSelectedListener(this);
//        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
//            @Override
//            public void onDrawerSlide(@NonNull View view, float v) {
//
//            }
//
//            @Override
//            public void onDrawerOpened(@NonNull View view) {
//
//            }
//
//            @Override
//            public void onDrawerClosed(@NonNull View view) {
//                if (mFragmentToSet != null) {
//                    addFragment(mFragmentToSet,true);
//                    mFragmentToSet=null;
//                }else if(logout){
//                    clearAllData();
//                }
//            }
//
//            @Override
//            public void onDrawerStateChanged(int i) {
//
//            }
//        });
//        this.getSupportFragmentManager().addOnBackStackChangedListener(
//                new FragmentManager.OnBackStackChangedListener() {
//                    public void onBackStackChanged() {
//                        Fragment current = getCurrentFragment();
//                        if (current instanceof ListSurveyFragment) {
//                            navigationView.setCheckedItem(R.id.nav_survey);
//                        } else if(current instanceof MySurveyFragment) {
//                            navigationView.setCheckedItem(R.id.nav_mySurvey);
//                        } else if(current instanceof OnGoingFragment){
//                            navigationView.setCheckedItem(R.id.nav_ongoing);
//                        }
//                    }
//                });
//        navigationView.setCheckedItem(R.id.nav_survey);
        final int menu = getIntent().getExtras().getInt(MENU);
        switch (menu){
            case 1:
                addFragment(new ListSurveyFragment(),false);
                toolbar_title.setText("List Survey");
                break;
            case 2:
                addFragment(new OnGoingFragment(),false);
                toolbar_title.setText("On Going Survey");
                break;
        }

    }


    private void addFragment(Fragment fragment,Boolean addToBackStack){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayoutMain, fragment);
        if(addToBackStack)fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            mFragmentToSet = null;
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id){
            case R.id.action_search:
                //startActivity(new Intent(this,SearchActivity.class));
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



    public Fragment getCurrentFragment() {
        return this.getSupportFragmentManager().findFragmentById(R.id.frameLayoutMain);
    }


    @Override
    public void onChangeFragment(int fragment) {
        switch (fragment){
            case 1:
                break;
            case 2:
                addFragment(new MySurveyFragment(),false);
                toolbar_title.setText("Survey anda");
                //navigationView.setCheckedItem(R.id.nav_mySurvey);
                break;
        }
    }

}

