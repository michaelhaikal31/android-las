package com.dak.las.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import com.dak.las.R
import com.dak.las.interfaces.ApiService
import com.dak.las.services.RetrofitService
import com.dak.las.sessions.DeviceSession
import com.dak.las.sessions.UserSession
import com.dak.las.utilities.Constant
import com.dak.las.utilities.ConstantString
import com.dak.las.utilities.SuperDialog
import com.rengwuxian.materialedittext.MaterialEditText
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

class LoginActivity : AppCompatActivity(){

    private var txtMsisdn: MaterialEditText? = null
    private var btnLogin: Button? = null
    private var progressBar: ProgressBar? = null
    private val ARG_PARAM1 = "msisdn"
    private val ARG_PARAM2 = "name"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val userSession = UserSession()
        val deviceSession = DeviceSession(this@LoginActivity)
        if(userSession.getLoginState(this@LoginActivity) && deviceSession.isInitialized){
            startActivity(Intent(this@LoginActivity,HomeActivity::class.java))
            finish()
        } else if (!deviceSession.isInitialized && userSession.getLoginState(this)!!) run {
            startActivity(Intent(this@LoginActivity, InitializingActivity::class.java))
            finish()
        }else {
            setContentView(R.layout.activity_login)
            btnLogin = findViewById(R.id.btnLogin)
            txtMsisdn = findViewById(R.id.materialEditText2)
            progressBar = findViewById(R.id.pbLogin)
            btnLogin!!.setOnClickListener {
                if (txtMsisdn!!.getText()!!.length == 0) {
                    SuperDialog.showDialogMessage(this@LoginActivity, "", ConstantString.PHONE_NUMBER_LENGTH)
                } else if (txtMsisdn!!.getText()!!.length < 11) {
                    SuperDialog.showDialogMessage(this@LoginActivity, "", ConstantString.PHONE_NUMBER_LENGTH)
                } else {
                    checkMsisdn()
                }
            }
        }
    }

    fun checkMsisdn(){
        progressBar?.visibility = View.VISIBLE
        btnLogin?.isEnabled = false
        val key = Constant.getKey(24)
        var fixNumber :String
        val apiInterface = RetrofitService.getRetrofit(key).create(ApiService::class.java)
        val paramObject = JSONObject()
        try{
            fixNumber = txtMsisdn?.getText()!!.toString()[0].toString()
            if (fixNumber.equals("0", ignoreCase = true)) {
                val newPhone = StringBuilder(txtMsisdn!!.getText()!!)
                newPhone.deleteCharAt(0)
                fixNumber = "62$newPhone"
            } else {
                fixNumber = txtMsisdn!!.getText()!!.toString()
            }
            println("asd $fixNumber")
            paramObject.put("msisdn", fixNumber)
        }catch (e : Exception){
            println("asd [Login] "+e.message)
        }
        val call = apiInterface.check_msisdn(paramObject.toString(),"{}")
        call.enqueue(object: Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                progressBar?.visibility=View.INVISIBLE
                SuperDialog.showDialogMessage(this@LoginActivity,Constant.TITLE_CANNOT_CONNECT,Constant.TITLE_CANNOT_CONNECT)
                println("asd failure [login] "+t.message)
                btnLogin?.isEnabled = true
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    //System.out.println("asd respon "+tripleDes.decrypt(response.toString()));
                    val jsonObject = JSONObject(response.body())
                    if (jsonObject.getString("rc") == "00") {
                        val intent = Intent(this@LoginActivity, EnterPinActivity::class.java)
                        intent.putExtra(ARG_PARAM2, jsonObject.getJSONObject("data").getString("name"))
                        intent.putExtra(ARG_PARAM1, jsonObject.getString("msisdn"))
                        startActivity(intent)
                        finish()
                    } else {
                        SuperDialog.showDialogMessage(this@LoginActivity, "", jsonObject.getString("rm"))
                    }
                } catch (e: Exception) {
                    SuperDialog.showDialogMessage(this@LoginActivity, Constant.TITLE_SERVER_RESPONSE_FAILED, Constant.DESC_SERVER_RESPONSE_FAILED)
                    println("asd [login] catch " + e.toString())
                }

                progressBar?.setVisibility(View.INVISIBLE)
                btnLogin?.setEnabled(true)
            }
        })

    }
}