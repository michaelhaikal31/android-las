package com.dak.las.activities

import RetrofitServiceKotlin
import android.os.Build
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.WindowManager
import com.dak.las.R
import com.dak.las.adapters.DynamicResultAdapter
import com.dak.las.fragments.ListSurveyFragment
import com.dak.las.interfaces.ApiServiceKotlin
import com.dak.las.models.ValueDynamicResult
import com.dak.las.utilities.SuperDialog
import kotlinx.android.synthetic.main.activity_detail_inventory.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailInventoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
        }
        setContentView(R.layout.activity_detail_inventory)
        val container : ConstraintLayout = findViewById(R.id.containerDetailInventory)
        setSupportActionBar(toolbarDetailInventory)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        getDetailInventory(intent.getStringExtra(ListSurveyFragment.PARAM_ID_TRX))
    }

    fun getDetailInventory(id_survey:String){
        val superDialog = SuperDialog(this@DetailInventoryActivity)
        superDialog.createProgressDialog("Please wait..",layoutInflater)
        superDialog.show()
        println("asd "+id_survey)
        val service = RetrofitServiceKotlin.getRetrofit().create(ApiServiceKotlin::class.java)

        val jsonParam  = JSONObject()
        try{
            jsonParam.put("msisdn","62895376559480")
            jsonParam.put("id",id_survey)
        }catch (e : Exception){
            println("asd exception when creating json param "+e.message)
        }
        println("asd param "+jsonParam.toString())
        val call = service.getDetailSurvey(jsonParam.toString(),"{}")
        call.enqueue(object : Callback<ValueDynamicResult> {
            override fun onFailure(call: Call<ValueDynamicResult>, t: Throwable) {
                println("asd error "+t.message)
                superDialog.dismis()

            }

            override fun onResponse(call: Call<ValueDynamicResult>, response: Response<ValueDynamicResult>) {
                rvDetailInventory.apply {
                    layoutManager = LinearLayoutManager(this@DetailInventoryActivity)
                    adapter = DynamicResultAdapter(response.body()!!.data)
                    addItemDecoration(DividerItemDecoration(rvDetailInventory.context,DividerItemDecoration.VERTICAL))
                }
                superDialog.dismis()
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
