package com.dak.las.activities

import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.RelativeLayout
import com.dak.las.models.DrawingClass
import kotlinx.android.synthetic.main.activity_draw_signature.*
import java.io.File
import java.io.FileOutputStream
import java.util.*
import android.content.Intent
import android.net.Uri
import com.dak.las.R


class DrawSignatureActivity : AppCompatActivity() {

    private var view : View? = null
    private var bitmap : Bitmap? = null
    private var canvas : Canvas? = null
    private var path2 : Path? = null
    private var paint : Paint? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_draw_signature)
        view = SketchSheetView(this@DrawSignatureActivity)
        paint = Paint()
        path2 = Path()
        canvasLayout.addView(view, RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT))

        paint?.isDither
        paint?.setColor(Color.BLACK)
        paint?.setStyle(Paint.Style.STROKE)
        paint?.setStrokeJoin(Paint.Join.ROUND)
        paint?.setStrokeCap(Paint.Cap.ROUND)
        paint?.setStrokeWidth(4f)
        btnClearSignature.setOnClickListener {
            val bitmap = Bitmap.createBitmap(canvasLayout.getWidth(), canvasLayout.getHeight(), Bitmap.Config.ARGB_4444)
            val canvas = Canvas(bitmap)
            val bgDrawable = canvasLayout.getBackground()
            if (bgDrawable != null) {
                bgDrawable.draw(canvas)
            } else {
                canvas.drawColor(Color.WHITE)
            }
            canvasLayout.draw(canvas)
            saveToDisk(bitmap)
            path2?.reset()
            view?.invalidate()

        }

    }


    fun saveToDisk(bitmap : Bitmap){
        val root = Environment.getExternalStorageDirectory().toString()
        val myDir = File(root + "/req_images")
        myDir.mkdirs()
        val generator = Random()
        var n = 10000
        n = generator.nextInt(n)
        val fname = "Image-$n.jpg"
        val file = File(myDir, fname)
        if (file.exists())
            file.delete()
        try {
            val out = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
            out.flush()
            out.close()
            sendBroadcast(Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())))
        }catch(e: Exception){
            println("asd save to disk "+e.message)
        }

    }

    inner class SketchSheetView : View{
        constructor(context: Context?) : super(context)
        init{
            bitmap = Bitmap.createBitmap(820,420,Bitmap.Config.ARGB_4444)
            canvas = Canvas(bitmap)
            this.setBackgroundColor(Color.WHITE)
        }

        val DrawingClassArrayList = ArrayList<DrawingClass>()

        override fun onTouchEvent(event: MotionEvent?): Boolean {
            val pathWithPaint = DrawingClass()
            canvas?.drawPath(path2,paint)
            if(event!!.action == MotionEvent.ACTION_DOWN){
                path2?.moveTo(event.x,event.y)
                path2?.lineTo(event.x,event.y)

            }else if(event.action == MotionEvent.ACTION_MOVE){
                path2?.lineTo(event.x,event.y)
                pathWithPaint.DrawingClassPath=path2
                pathWithPaint.DrawingClassPaint=paint
                DrawingClassArrayList.add(pathWithPaint)
            }
            invalidate()
            return true
        }

        override fun onDraw(canvas: Canvas?) {
            super.onDraw(canvas)
            if(DrawingClassArrayList.size>0){
                canvas?.drawPath(
                       DrawingClassArrayList.get(DrawingClassArrayList.size-1).DrawingClassPath,
                        DrawingClassArrayList.get(DrawingClassArrayList.size-1).DrawingClassPaint
                )
            }
        }
    }
}
