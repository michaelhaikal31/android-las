//class MainActivity : AppCompatActivity() {
//
//    internal var relativeLayout: RelativeLayout? = null
//    internal var paint: Paint? = null
//    internal var view: View? = null
//    internal var path2: Path? = null
//    internal var bitmap: Bitmap? = null
//    internal var canvas: Canvas? = null
//    internal var button: Button? = null
//
//    protected fun onCreate(savedInstanceState: Bundle) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        relativeLayout = findViewById(R.id.relativelayout1) as RelativeLayout
//
//        button = findViewById(R.id.button) as Button
//
//        view = SketchSheetView(this@MainActivity)
//
//        paint = Paint()
//
//        path2 = Path()
//
//        relativeLayout!!.addView(view, LayoutParams(
//                RelativeLayout.LayoutParams.MATCH_PARENT,
//                RelativeLayout.LayoutParams.MATCH_PARENT))
//
//        paint!!.setDither(true)
//
//        paint!!.setColor(Color.parseColor("#000000"))
//
//        paint!!.setStyle(Paint.Style.STROKE)
//
//        paint!!.setStrokeJoin(Paint.Join.ROUND)
//
//        paint!!.setStrokeCap(Paint.Cap.ROUND)
//
//        paint!!.setStrokeWidth(2)
//
//        button!!.setOnClickListener(object : View.OnClickListener() {
//            fun onClick(v: View) {
//
//                path2!!.reset()
//
//            }
//        })
//
//    }
//
//    internal inner class SketchSheetView(context: Context) : View(context) {
//
//        private val DrawingClassArrayList = ArrayList<DrawingClass>()
//
//        InitializingActivity {
//
//            bitmap = Bitmap.createBitmap(820, 480, Bitmap.Config.ARGB_4444)
//
//            canvas = Canvas(bitmap)
//
//            this.setBackgroundColor(Color.WHITE)
//        }
//
//        fun onTouchEvent(event: MotionEvent): Boolean {
//
//            val pathWithPaint = DrawingClass()
//
//            canvas!!.drawPath(path2, paint)
//
//            if (event.getAction() === MotionEvent.ACTION_DOWN) {
//
//                path2!!.moveTo(event.getX(), event.getY())
//
//                path2!!.lineTo(event.getX(), event.getY())
//            } else if (event.getAction() === MotionEvent.ACTION_MOVE) {
//
//                path2!!.lineTo(event.getX(), event.getY())
//
//                pathWithPaint.path = path2
//
//                pathWithPaint.paint = paint
//
//                DrawingClassArrayList.add(pathWithPaint)
//            }
//
//            invalidate()
//            return true
//        }
//
//        protected fun onDraw(canvas: Canvas) {
//            super.onDraw(canvas)
//            if (DrawingClassArrayList.size() > 0) {
//
//                canvas.drawPath(
//                        DrawingClassArrayList[DrawingClassArrayList.size() - 1].getPath(),
//
//                        DrawingClassArrayList[DrawingClassArrayList.size() - 1].getPaint())
//            }
//        }
//    }
//
//    inner class DrawingClass {
//
//        var path: Path? = null
//        var paint: Paint? = null
//    }
//
//}