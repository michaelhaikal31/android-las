package com.dak.las.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.dak.las.R
import kotlinx.android.synthetic.main.activity_about.*

class AboutActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        imageView3.setOnClickListener {
            val t = Toast.makeText(this@AboutActivity,"clicked",Toast.LENGTH_SHORT)
            t.show()
            setImageResource()
        }
    }

    fun setImageResource(){
        imageView3.setImageResource(R.drawable.ic_account_circle)
    }
}
