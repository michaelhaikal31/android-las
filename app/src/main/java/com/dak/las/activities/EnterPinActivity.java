package com.dak.las.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.dak.las.R;
import com.dak.las.interfaces.ApiService;
import com.dak.las.services.RetrofitService;
import com.dak.las.sessions.UserSession;
import com.dak.las.utilities.Constant;
import com.dak.las.utilities.SuperDialog;
import com.dak.las.utilities.TripleDes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterPinActivity extends AppCompatActivity {
    private static final String ARG_PARAM1 = "msisdn";
    private static final String ARG_PARAM2 = "name";
    private String mParam1;
    private String mParam2;
    private TripleDes tripleDesPin;
    private Button btnProses;
    private PinEntryEditText otpEditText;
    private TextView txtUserName;
    private ProgressBar progressBar;
    private String imei="";
    private String token="";
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE =123 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_pin);
        makeTransparentAppBar();
        getImei();
        getToken();
        if(getIntent().getExtras()!=null){
            mParam1 = getIntent().getExtras().getString(ARG_PARAM1);
            mParam2 = getIntent().getExtras().getString(ARG_PARAM2);
        }
        btnProses = findViewById(R.id.btnProsesOtp);
        otpEditText = findViewById(R.id.txt_pin_entry);
        txtUserName = findViewById(R.id.txtUserName);
        progressBar = findViewById(R.id.pbEnterPin);
        txtUserName.setText(mParam2);
        btnProses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        otpEditText.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
            @Override
            public void onPinEntered(CharSequence str) {
                System.out.println("asd char "+str.length());

            }
        });
        otpEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (otpEditText.getText().length() == 6) {
                    btnProses.setEnabled(true);
                    btnProses.setBackground(getResources().getDrawable(R.drawable.gradient_color));
                } else {
                    btnProses.setEnabled(false);
                    btnProses.setBackgroundColor(getResources().getColor(R.color.whiteDark4));
                }

            }
        });

    }

    private void getToken(){
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    System.out.println("asd [Login] getInstanceId failed"+ task.getException());
                    return;
                }
                // Get new Instance ID token
                token = task.getResult().getToken();
                System.out.println("asd [Login] token : "+token);
            }
        });

    }

    private void getImei() {
        if (ActivityCompat.checkSelfPermission(EnterPinActivity.this, Manifest.permission.READ_PHONE_STATE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
            return;
        }
        TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (mTelephony.getPhoneCount() == 2) {
                    imei = mTelephony.getImei(0);
                }else{
                    imei = mTelephony.getImei();
                }
            }else{
                if (mTelephony.getPhoneCount() == 2) {
                    imei = mTelephony.getDeviceId(0);
                } else {
                    imei = mTelephony.getDeviceId();
                }
            }
        } else {
            imei = mTelephony.getDeviceId();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==MY_PERMISSIONS_REQUEST_READ_PHONE_STATE){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getImei();
            } else {
                finish();
            }
        }
    }

    private void makeTransparentAppBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarEnterPin);

        // Toolbar :: Transparent
        toolbar.setBackgroundColor(Color.TRANSPARENT);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Status bar :: Transparent
        Window window = this.getWindow();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

    }

    private void login(){
        progressBar.setVisibility(View.VISIBLE);
        final String key = Constant.getKey(24);
        ApiService apiInterface = RetrofitService.getRetrofit(key).create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try{
            tripleDesPin = new TripleDes(Constant.KEY_DES_PIN);
            //tripleDesJSon = new TripleDes(key);
            paramObject.put("msisdn",mParam1);
            paramObject.put("pin",tripleDesPin.encrypt(otpEditText.getText().toString()));
            paramObject.put("imei",imei);
            paramObject.put("token",token);
            //encryptedBody = tripleDesJSon.encrypt(paramObject.toString());
        }catch (JSONException e){
        }catch (Exception e){

        }
        System.out.println("asd login "+paramObject);
        //Call<String> call = apiInterface.login(encryptedBody);
        Call<String> call = apiInterface.login(paramObject.toString(),"{}");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    JSONObject jsonObject = new JSONObject(response.body());
                    System.out.println("asd response[EnterPIn] "+jsonObject.toString());
                    if(jsonObject.getString("rc").equals("00")){
                        UserSession userSession = new UserSession();
                        userSession.setName(EnterPinActivity.this,jsonObject.getJSONObject("data").getString("name"));
                        userSession.setMsiSdn(EnterPinActivity.this,jsonObject.getString("msisdn"));
                        userSession.setLoginState(EnterPinActivity.this,true);
                        userSession.setAccountType(EnterPinActivity.this,jsonObject.getJSONObject("data").getString("account_type"));
                        Intent intent = new Intent(EnterPinActivity.this, InitializingActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        SuperDialog.showDialogMessage(EnterPinActivity.this,"",jsonObject.getString("rm"));
                    }
                }catch (Exception e){
                    SuperDialog.showDialogMessage(EnterPinActivity.this,"Failed","Cannot process server's response !");
                    System.out.println("asd "+e.toString());


                }
                progressBar.setVisibility(View.INVISIBLE);
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                SuperDialog.showDialogMessage(EnterPinActivity.this,"Failed","Oops, Something wrong please try again later");
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent(EnterPinActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
