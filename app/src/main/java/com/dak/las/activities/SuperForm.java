package com.dak.las.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.Base64;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dak.las.BuildConfig;
import com.dak.las.R;
import com.dak.las.interfaces.ApiService;
import com.dak.las.models.DynamicView.DynamicView;
import com.dak.las.models.Form.Form;
import com.dak.las.models.ImageUpload;
import com.dak.las.models.ListMenu.ListMenu;
import com.dak.las.models.StaticJSON;
import com.dak.las.models.SuperDialog.InputAutoCompleteText;
import com.dak.las.models.SuperDialog.InputCheckBox;
import com.dak.las.models.SuperDialog.InputOption;
import com.dak.las.models.SuperDialog.InputRadio;
import com.dak.las.models.SuperDialog.InputText;
import com.dak.las.services.RetrofitService;
import com.dak.las.sessions.DeviceSession;
import com.dak.las.sessions.UserSession;
import com.dak.las.utilities.Constant;
import com.dak.las.utilities.DatePickerHelper;
import com.dak.las.utilities.LasDatabase;
import com.dak.las.utilities.LocationHelper;
import com.dak.las.utilities.SuperDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.adapters.FastItemAdapter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuperForm extends AppCompatActivity{
    private final static int REQUEST_GALERY =321 ;
    public final static String ARG_PARAM1 = "id_json_view";
    public final static String ARG_PARAM2 = "id_survey";
    public final static String ARG_PARAM3 = "insert_data";
    public final static String ARG_PARAM4 = "online";
    public final static String ARG_PARAM5 = "id_trx";
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS =321;
    private ScrollView scrollView;
    private List<InputText> allEds;
    private List<InputRadio> allRadio;
    private List<InputCheckBox> allCheck;
    private List<InputOption> allOption;
    private JSONObject object = new JSONObject();
    private JSONArray jsonForm;
    private ImageView imageview;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private int imageForLoad;
    private String imageNameForSend;
    private JSONArray paramImage;
    private Button btn;
    private HashMap<Integer, String> map = new HashMap<>();
    private LasDatabase db;
    private DeviceSession deviceSession;
    private DynamicView dynamicView;
    private ArrayList<ImageUpload> listImage;
    private List<String> idImageUpload= new ArrayList<>();
    private String from;
    private String id_survey;
    private Boolean online;
    private int imgJsonPos;
    private String id_trx="";
    private Form form;
    private String mCurrentPhotoPath;
    public static final String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    private LocationManager locationManager;
    String[] languages = { "C","C++","Java","C#","PHP","JavaScript","jQuery","AJAX","JSON" };
    private ArrayList<InputAutoCompleteText> allAcEds;
    public AutoCompleteTextView ac_edittext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        setContentView(R.layout.activity_super_form);
        final Toolbar toolbar = findViewById(R.id.toolbarSuperForm);
        scrollView = findViewById(R.id.scroolView);
        btn = findViewById(R.id.btn);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        db = Room.databaseBuilder(getApplicationContext(),
                LasDatabase.class, "db_las").allowMainThreadQueries().build();
        final Bundle bundle = getIntent().getExtras();
        paramImage = new JSONArray();
        listImage = new ArrayList<>();
        dynamicView = new DynamicView();
        System.out.println("asd PARAM id JSON VIEW "+bundle.getString(ARG_PARAM1));
        System.out.println("asd PARAM id_survey "+bundle.getString(ARG_PARAM2));
        System.out.println("asd PARAM insert_data "+bundle.getString(ARG_PARAM3));
        System.out.println("asd PARAM online "+bundle.getString(ARG_PARAM4));
        System.out.println("asd PARAM id_trx "+bundle.getString(ARG_PARAM5));
        if(bundle!=null){
            online = bundle.getBoolean(ARG_PARAM4);
            id_trx = bundle.getString(ARG_PARAM5);
            id_survey = bundle.getString(ARG_PARAM2);
            dynamicView = db.dynamicViewDao().getView(Integer.valueOf(bundle.getString(ARG_PARAM1)));
            form = db.formDao().getView(bundle.getString(ARG_PARAM1),id_survey);
            if(form!=null){
                System.out.println("asd data form ada berdasarkan id view dan id survey"+form.toString());
                dynamicView.setJsonView(form.getJsonForm());
            }
            if(bundle.getString(ARG_PARAM3)!=null){
                System.out.println("asd isi jsonView dari dynamicView object dengan param data inser");
                dynamicView.setJsonView(bundle.getString(ARG_PARAM3));
            }
            from = dynamicView.getFrom();
        }else{
            from="0";
            deviceSession = new DeviceSession(this);
            dynamicView = db.dynamicViewDao().getView(deviceSession.getMainActivity());
        }
        if(dynamicView.getJenis().contains("menu")){
            //tipe menu
            scrollView.addView(generateMenuView(dynamicView.getJsonView()));
            getSupportActionBar().setTitle(dynamicView.getViewTitle());
            btn.setVisibility(View.GONE);
        }else if(dynamicView.getJenis().equals("form")){
            //tipe form
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialog("Kirim form","Pastikan anda sudah mengecek kelengkapan setiap kolom, simpan sekarang ?");
                }
            });
            getSupportActionBar().setTitle(dynamicView.getViewTitle());
            scrollView.addView(generateFormView(dynamicView.getJsonView()));
        }else{

        }
        requestPermission();

    }



    private void requestPermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                &&  ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions,
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        final String imageFileName = "JPEG_" + timeStamp + "_";
        final File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        final File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//        }
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            // Error occurred while creating the File
            System.out.println("asd exception "+ex.getMessage());
            SuperDialog.showDialogMessage(this,"Request permission","Untuk dapat mengambil gambar kami perlu izin anda untuk mengakses fungsi kamera, silahkan aktifkan permission kamera untuk aplikasi LAS di pengaturan perangkat anda");

            }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                    FileProvider.getUriForFile(this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            photoFile));
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_IMAGE_CAPTURE) {
            final File imgFile = new File(mCurrentPhotoPath);
            final Bitmap imageBitmap;
            if(imgFile.exists()){
                 imageBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                //Drawable d = new BitmapDrawable(getResources(), myBitmap);

            final String base64 = encodeFromString(imageBitmap);
            map.put(imageForLoad, base64);
            imageview = findViewById(imageForLoad);
            imageview.setImageBitmap(imageBitmap);
            try{
                jsonForm.getJSONObject(imgJsonPos).put("value", base64);
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    final LocationHelper locationHelper = new LocationHelper(SuperForm.this, new LocationHelper.LocationHelperInterface() {
                        @Override
                        public void onLocationReady(String lat, String longi) {
                            try {
                                jsonForm.getJSONObject(imgJsonPos).put("latitude", lat);
                                jsonForm.getJSONObject(imgJsonPos).put("longitude", longi);
                            }catch (Exception e){
                                System.out.println("asd exception[SuperForm-activity result] "+e.getMessage());
                            }
                        }
                    });
                }else{
                    jsonForm.getJSONObject(imgJsonPos).put("latitude", "-");
                    jsonForm.getJSONObject(imgJsonPos).put("longitude", "-");
                    SuperDialog.showDialogMessage(SuperForm.this,"Gps non-aktif","Silahkan aktifkan GPS di smartphone anda agar kami dapat melacak lokasi telepon anda !");
                }
            }catch (Exception e){

            }

            }else{
                Toast.makeText(this,"Image doesn exist",Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode==REQUEST_GALERY){
            try {
                final Uri uri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(uri);
                final Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
                imageview = findViewById(imageForLoad);
                imageview.setImageBitmap(decodeBase64(StaticJSON.base64));
            }catch (Exception e){
                System.out.println("asd failure "+e.toString());
            }
        }
    }

    public static Bitmap decodeBase64(String input){
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static String encodeFromString(Bitmap bm){
        try {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT);
        }catch (Exception e){
            System.out.println("gelo "+e.toString());
            return null;
        }
    }

    private void pickFromGallery(){
        //Create an Intent with action as ACTION_PICK
        final Intent intent=new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
        // Launching the Intent
        startActivityForResult(intent,REQUEST_GALERY);

    }

    private RecyclerView generateMenuView(String arrayMenu){
        final FastItemAdapter<ListMenu> fastItemAdapter;
        final ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        final RecyclerView recyclerView = new RecyclerView(this);
        recyclerView.setLayoutParams(params);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        fastItemAdapter = new FastItemAdapter<>();
        recyclerView.setAdapter(fastItemAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        try{
            fastItemAdapter.add(generateListMenu(new JSONArray(arrayMenu)));
        }catch (Exception e){
            SuperDialog.showDialogMessage(this,"Server response error","Please try again later");
        }
        fastItemAdapter.withSelectable(true);
        fastItemAdapter.withOnClickListener(new FastAdapter.OnClickListener<ListMenu>() {
            @Override
            public boolean onClick(View v, IAdapter<ListMenu> adapter, ListMenu item, int position) {
                if(item.getJenis() && online){
                    System.out.println("asd jenis form");
                    getFilledData(item.getTo());
                }else{
                    System.out.println("asd== "+item.toString());
                    db.close();
                    final Intent intent = new Intent(SuperForm.this, SuperForm.class);
                    intent.putExtra(ARG_PARAM1, item.getTo());
                    intent.putExtra(ARG_PARAM2, id_survey);
                    startActivity(intent);
                    finish();

                }

                return false;
            }
        });

        return recyclerView;
    }

    private ArrayList<ListMenu> generateListMenu(JSONArray arrayMenu){
        final ArrayList<ListMenu> list = new ArrayList<>();
        JSONObject menu;
        try {
            for (int i = 0; i < arrayMenu.length(); i++) {
                menu = arrayMenu.getJSONObject(i);
                list.add(new ListMenu(i,menu.getString("header"),menu.getString("description"),menu.getString("to"),menu.getBoolean("jenis"),menu.getBoolean("mandatory")));
            }
        }catch (Exception e){
            System.out.println("asd exception [SuperForm.generateListMenu] "+e.toString());
        }
        return list;
    }

    private LinearLayout generateFormView(String json){
        final ViewGroup.LayoutParams paramsRoot = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        final LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(paramsRoot);
        linearLayout.setFocusableInTouchMode(true);
//        LinearLayout.LayoutParams marginParam = new LinearLayout.LayoutParams(linearLayout.getLayoutParams());
//        marginParam.setMargins(50,0,50,0);
//        linearLayout.setLayoutParams(marginParam);

        EditText ed;
        allAcEds = new ArrayList<>();
        allEds = new ArrayList<>();
        allRadio = new ArrayList<>();
        allCheck = new ArrayList<>();
        allOption = new ArrayList<>();
        try {
            jsonForm = new JSONArray(json);
            final int size = jsonForm.length();
            for(int i=0;i<size;i++){
                object = jsonForm.getJSONObject(i);
                if(object.getString("type").contains("ac_")){
                    final ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final TextView textView = new TextView(this);
                    textView.setLayoutParams(params);
                    textView.setText(object.getString("label"));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(textView.getLayoutParams());
                    layoutParams.setMargins(0,50,0,0);
                    textView.setLayoutParams(layoutParams);
                    linearLayout.addView(textView);

                    ac_edittext = new AutoCompleteTextView(this);
                    ac_edittext.setLayoutParams(params);
                    ac_edittext.setId(i);
                    final ArrayList<String> options = new ArrayList<>();
                    final JSONArray array = new JSONArray(object.getString("options"));
                    for(int q=1;q<= array.getJSONObject(0).length(); q++){
                        options.add(array.getJSONObject(0).getString("option"+q));
                    }

                    final ArrayAdapter adapter = new ArrayAdapter<>(this,android.R.layout.simple_dropdown_item_1line,options);
                    ac_edittext.setAdapter(adapter);

//                    if(object.getString("type").substring(11).equals("(1)")) {
//                        ac_edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                            @Override
//                            public void onFocusChange(View v, boolean hasFocus) {
//                                if (hasFocus) {
//                                    System.out.println("asd lolos "+ac_edittext.getId());
//                                    ac_edittext.showDropDown();
//                                }
//                                System.out.println("asd focus : " + v.getId());
//                            }
//                        });
//                    }
                    if(object.getString("type").substring(11).equals("(1)")) {
                        ac_edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View v, boolean hasFocus) {
                                if(hasFocus){
                                    final int size = allAcEds.size();
                                    for(int q=0;q<size;q++){
                                        if(allAcEds.get(q).getEditText().getId()==v.getId()){
//                                            allAcEds.get(q).getEditText().showDropDown();
                                        }
                                    }
                                }
                              }
                        });
                    }
                    try {
                        ac_edittext.setText(object.getString("value"));
                    }catch (Exception e){

                    }
                    //ed.setHint(object.getString("label"));

                    layoutParams = new LinearLayout.LayoutParams(ac_edittext.getLayoutParams());
                    layoutParams.setMargins(0,0,0,0);
                    ac_edittext.setLayoutParams(layoutParams);
                    if(object.getString("input_type").equalsIgnoreCase("number")){
                        ac_edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
                    }
                    ac_edittext.setFilters(new InputFilter[]{new InputFilter.LengthFilter(object.getInt("length"))});
                    allAcEds.add(new InputAutoCompleteText(ac_edittext,i));
                    //il.addView(ed);
                    linearLayout.addView(ac_edittext);
                }

                if(object.getString("type").equalsIgnoreCase("textfield")){
                    final ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final TextView textView = new TextView(this);
                    textView.setLayoutParams(params);
                    textView.setText(object.getString("label"));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(textView.getLayoutParams());
                    layoutParams.setMargins(0,50,0,0);
                    textView.setLayoutParams(layoutParams);
                    linearLayout.addView(textView);

                    ed = new EditText(this);
                    ed.setLayoutParams(params);
                    try {
                        ed.setText(object.getString("value"));
                    }catch (Exception e){

                    }
                    //ed.setHint(object.getString("label"));
                    layoutParams = new LinearLayout.LayoutParams(ed.getLayoutParams());
                    layoutParams.setMargins(0,0,0,0);
                    ed.setLayoutParams(layoutParams);
                    if(object.getString("input_type").equalsIgnoreCase("number")){
                        ed.setInputType(InputType.TYPE_CLASS_NUMBER);
                    }
                    ed.setFilters(new InputFilter[]{new InputFilter.LengthFilter(object.getInt("length"))});
                    allEds.add(new InputText(ed,i));
                    //il.addView(ed);
                    linearLayout.addView(ed);
                }

                if(object.getString("type").equalsIgnoreCase("textarea")){
                    final ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200);
                    ed = new EditText(this);
                    ed.setHint(object.getString("label"));
                    try {
                        ed.setText(object.getString("value"));
                    }catch (Exception e){

                    }
                    ed.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                    ed.setPadding(10,5,10,5);
                    ed.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
                    ed.setGravity(Gravity.LEFT | Gravity.TOP);
                    ed.setBackgroundResource(R.drawable.text_area_bg);
                    ed.setLayoutParams(params);
                    ed.setFilters(new InputFilter[]{new InputFilter.LengthFilter(object.getInt("length"))});
                    final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ed.getLayoutParams());
                    layoutParams.setMargins(0,50,0,0);
                    ed.setLayoutParams(layoutParams);
                    allEds.add(new InputText(ed,i));
                    linearLayout.addView(ed);
                }

                if(object.getString("type").equalsIgnoreCase("radio_button")){
                    final ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final TextView textView = new TextView(this);
                    textView.setLayoutParams(params);
                    textView.setText(object.getString("label"));
                    final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(textView.getLayoutParams());
                    layoutParams.setMargins(10,50,0,20);
                    textView.setLayoutParams(layoutParams);
                    linearLayout.addView(textView);
                    final RadioGroup radioGroup = new RadioGroup(this);
                    radioGroup.setId(View.generateViewId());
                    final JSONArray array = new JSONArray(object.getString("options"));
                    for (int row = 0; row < 1; row++) {
                        radioGroup.setOrientation(LinearLayout.VERTICAL);
                        for (int j = 1; j <= array.getJSONObject(0).length(); j++) {
                            RadioButton rdbtn = new RadioButton(this);
                            rdbtn.setId(View.generateViewId());
                            allRadio.add(new InputRadio(rdbtn,i,i));
                            rdbtn.setText(array.getJSONObject(0).getString("option"+j));
                            try {
                                if (rdbtn.getText().equals(object.getString("value"))) {
                                    rdbtn.setChecked(true);
                                }
                            }catch (Exception e){

                            }
                            radioGroup.addView(rdbtn);
                        }
                        linearLayout.addView(radioGroup);
                    }//end for add radio to group
                }

                if(object.getString("type").equalsIgnoreCase("check_box")){
                    final ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final CheckBox checkBox = new CheckBox(this);
                    checkBox.setLayoutParams(params);
                    checkBox.setId(i);
                    final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(checkBox.getLayoutParams());
                    layoutParams.setMargins(0,50,0,0);
                    checkBox.setLayoutParams(layoutParams);
                    checkBox.setText(object.getString("label"));
                    allCheck.add(new InputCheckBox(checkBox,i));
                    try{
                        System.out.println("asd is true");
                        if(object.getString("value").equals("true"))checkBox.setChecked(true);
                    }catch (Exception e){

                    }
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            try {
                                if (isChecked) {
                                    jsonForm.getJSONObject(buttonView.getId()).put("value","true");
                                } else {
                                    jsonForm.getJSONObject(buttonView.getId()).put("value","false");
                                }
                            }catch (JSONException e){

                            }
                        }
                    });
                    linearLayout.addView(checkBox);
                }//end if checkbox tipe

                if(object.getString("type").equalsIgnoreCase("option")){
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final TextView textView = new TextView(this);
                    textView.setLayoutParams(params);
                    textView.setText(object.getString("label"));
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(textView.getLayoutParams());
                    layoutParams.setMargins(10,50,0,0);
                    textView.setLayoutParams(layoutParams);
                    linearLayout.addView(textView);

                    params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    Spinner spinner = new Spinner(this);
                    spinner.setLayoutParams(params);
                    ArrayList<String> options = new ArrayList<>();
                    JSONArray array = new JSONArray(object.getString("options"));
                    for(int q=1;q<= array.getJSONObject(0).length(); q++){
                        options.add(array.getJSONObject(0).getString("option"+q));
                    }
                    ArrayAdapter<String> adapterOpt = new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,options);
                    spinner.setAdapter(adapterOpt);
                    layoutParams = new LinearLayout.LayoutParams(spinner.getLayoutParams());
                    layoutParams.setMargins(0,0,0,0);
                    spinner.setLayoutParams(layoutParams);
                    try {
                        int spinnerPosition = adapterOpt.getPosition(object.getString("value"));
                        spinner.setSelection(spinnerPosition);
                    }catch (Exception e){

                    }
                    allOption.add(new InputOption(spinner,i,options));
                    linearLayout.addView(spinner);
                }//end option tipe

                if(object.getString("type").equalsIgnoreCase("image_upload")){
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    LinearLayout linearLayout1 = new LinearLayout(this);
                    linearLayout1.setOrientation(LinearLayout.HORIZONTAL);
                    final ImageView imgView = new ImageView(this);
                    imgView.setId(object.getInt("id"));
                    TextView textView = new TextView(this);
                    textView.setLayoutParams(params);

                    textView.setText(object.getString("label")+", ");
                    linearLayout1.addView(textView);
                    final Button button = new Button(this);
                    button.setLayoutParams(params);
                    button.setBackgroundColor(this.getResources().getColor(R.color.colorFullTransparent));
                    button.setTextColor(this.getResources().getColor(R.color.colorDarkBlue));
                    listImage.add(new ImageUpload(imgView.getId(),""));
                    final String imgName = object.getString("id");
                    final int pos = i;
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imageForLoad = imgView.getId();
                            imageNameForSend = imgName;
                            imgJsonPos = pos;
                            dispatchTakePictureIntent();

                        }
                    });
                    button.setText("Ambil gambar");
                    linearLayout1.addView(button);
                    linearLayout.addView(linearLayout1);
                    final LinearLayout linearLayout2 = new LinearLayout(this);
                    linearLayout2.setOrientation(LinearLayout.VERTICAL);
                    linearLayout2.setBackgroundResource(R.drawable.image_frame);
                    ViewGroup.LayoutParams paramsImage = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,600);
                    linearLayout2.setLayoutParams(paramsImage);
                    linearLayout2.setPadding(5,10,5,10);
                    paramsImage = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    imgView.setLayoutParams(paramsImage);
                    if(object.toString().contains("value")){
                        if(online && !object.getString("value").isEmpty()){
                            System.out.println("asd masuk glide");
                            Glide.with(this).load(object.getString("value")).into(imgView);
                        }else{
                            System.out.println("asd masuk set");
                            imgView.setImageBitmap(decodeBase64(object.getString("value")));
                        }
                    }else{
                        System.out.println("asd value gambar kosong");
                    }
                    linearLayout2.addView(imgView);
                    linearLayout.addView(linearLayout2);

                }

                if(object.getString("type").equalsIgnoreCase("datepicker")){
                    final ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    final TextView textView = new TextView(this);
                    textView.setText(object.getString("label"));
                    textView.setLayoutParams(params);
                    final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(textView.getLayoutParams());
                    layoutParams.setMargins(0,50,0,0);
                    textView.setLayoutParams(layoutParams);
                    linearLayout.addView(textView);
                    ed = new EditText(this);
                    ed.setId(ViewStub.generateViewId());
                    try {
                        System.out.println("asd picker masuk");
                        ed.setText(object.getString("value"));
                    }catch (Exception e){

                    }
                    ed.setClickable(true);
                    ed.setFocusable(true);
                    ed.setHint(object.getString("label"));
                    ed.setFocusableInTouchMode(false);
                    final ViewGroup.LayoutParams params2 = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    ed.setLayoutParams(params2);
                    ed.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_drop_down,0);
                    final DatePickerHelper dp = new DatePickerHelper(SuperForm.this,ed);
                    allEds.add(new InputText(ed,i));
                    linearLayout.addView(ed);
                }
            }//end for array json
        }catch (JSONException e){
            System.out.println("asd "+e.toString());
        }

        return linearLayout;
    }

    public JSONObject getValue(Boolean sendOnline){
        try {
            int size = allAcEds.size();
            for(int i=0;i<size;i++){
                jsonForm.getJSONObject(allAcEds.get(i).getPosition()).put("value",allAcEds.get(i).getEditText().getText().toString());
            }

            size = allEds.size();
            for(int i=0;i<allEds.size();i++){
                jsonForm.getJSONObject(allEds.get(i).getPosition()).put("value",allEds.get(i).getEditText().getText().toString());
            }

            size = allRadio.size();
            for(int i=0;i<allRadio.size();i++){
                if(allRadio.get(i).getRadioButton().isChecked()) {
                    jsonForm.getJSONObject(allRadio.get(i).getPostition()).put("value",allRadio.get(i).getRadioButton().getText());
                }
            }

            size = allOption.size();
            for (int i=0;i<allOption.size();i++){
                if(allOption.get(i).getSpinner().getSelectedItemId()>=0) {
                    jsonForm.getJSONObject(allOption.get(i).getPosition()).put("value", allOption.get(i).getLabel().get(allOption.get(i).getSpinner().getSelectedItemPosition()));
                }
            }

            final UserSession userSession = new UserSession();
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("msisdn",userSession.getMsiSdn(this));
            jsonObject.put("id_form",dynamicView.getTo());
            jsonObject.put("trx_web",id_survey);
//            if(sendOnline){
//                final int size = jsonForm.length();
//                for(int i=0;i<size;i++){
//                    if(jsonForm.getJSONObject(i).getString("type").equals("image_upload")){
//                        try{
//                            jsonForm.getJSONObject(i).put("value","");
//                        }catch (Exception e){
//
//                        }
//                    }
//                }
//                jsonObject.put("id",getIntent().getExtras().getInt(ARG_PARAM5));
//            }
            if(sendOnline) {
                jsonObject.put("id", getIntent().getExtras().getInt(ARG_PARAM5));
            }
            jsonObject.put("data",jsonForm);
            return jsonObject;
        }catch (JSONException e){
            System.out.println("asd json exception "+e.toString());
        }
        return null;
    }

    private void sendForm(){
        final SuperDialog progressBar = new SuperDialog(this);
        progressBar.createProgressDialog("Please wait",getLayoutInflater());
        progressBar.show();
        JSONObject jsonObject = new JSONObject();
        try{
            JSONArray data = new JSONArray();
            for(int i=0;i<listImage.size();i++){
                jsonObject = new JSONObject();
                jsonObject.put("id",listImage.get(i).getId());
                if(map.get(listImage.get(i).getId())==null){
                    jsonObject.put("base64","-");
                }else{
                    jsonObject.put("base64",map.get(listImage.get(i).getId()));
                }
                data.put(jsonObject);
            }
            jsonObject = new JSONObject();
            jsonObject.put("image",data);
            jsonObject.put(ARG_PARAM5,id_trx);
        }catch (Exception e){
            System.out.println("asd exception [superform:send form] : "+e.toString());
        }
        final ApiService apiService = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
        final Call<String> call = apiService.updateSurvey(getValue(true).toString(),jsonObject.toString());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    final JSONObject jsonResponse = new JSONObject(response.body());
                    if(jsonResponse.getString("rc").equals("00")){
                        progressBar.dismis();
                        SuperDialog.showDialogMessage(SuperForm.this,"Successfull","Data berhasil terkirim !");
                    }else{
                        progressBar.dismis();
                        SuperDialog.showDialogMessage(SuperForm.this,Constant.TITLE_CANNOT_CONNECT,jsonResponse.getString("em"));
                    }
                }catch (Exception e){
                    progressBar.dismis();
                    SuperDialog.showDialogMessage(SuperForm.this,Constant.TITLE_SERVER_RESPONSE_FAILED,Constant.DESC_SERVER_RESPONSE_FAILED);
                }

            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressBar.dismis();
                System.out.println("asd failure response call [SuperDIalog] "+call.toString());
                System.out.println("asd failure response throw [SuperDIalog] "+t.toString());

            }
        });
    }

    private void showDialog(String title,String message){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        if(online){
            builder.setPositiveButton("Update data", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendForm();
                }
            });
        }else{
//            builder.setPositiveButton("Kirim", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    sendForm();
//                }
//            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    return;
                }
            });
            builder.setNeutralButton("Save Offline", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    checkIfExist();
                }
            });
        }

        builder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(from.equals("0")){
            super.onBackPressed();
            finish();
        }else{
            db.close();
            final Intent intent = new Intent(SuperForm.this,SuperForm.class);
            intent.putExtra(ARG_PARAM1,from);
            intent.putExtra(ARG_PARAM2,id_survey);
            intent.putExtra(ARG_PARAM4,online);
            startActivity(intent);
            finish();
        }
    }

    private void checkIfExist(){
        new AsyncTask<Void,Void,Form>(){
            @Override
            protected Form doInBackground(Void... voids) {
                return db.formDao().getView(String.valueOf(dynamicView.getViewId()),id_survey);
            }

            @Override
            protected void onPostExecute(Form result) {
                super.onPostExecute(result);
                if(result!=null){
                    System.out.println("asd ada");
                    save(false);
                }else{
                    System.out.println("asd tidak ada");
                    save(true);
                }
            }
        }.execute();
    }

    private boolean delete(){
        try{
            db.formDao().delete(form);
            return true;
        }catch (Exception e){
            System.out.println("asd [SuperForm] exception delete failed");
           return false;
        }
    }

    private void save(final Boolean Empty){
        new AsyncTask<Void,Void,Boolean>(){
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    final Form formNew = new Form();
                    if(form!=null){
                        formNew.setId(form.getId());
                    }
                    System.out.println("asd te lewat");
                    formNew.setFormId(dynamicView.getViewId());
                    formNew.setSurveyId(Integer.valueOf(id_survey));
                    formNew.setJsonForm(getValue(false).getJSONArray("data").toString());
                    if(Empty){
                        System.out.println("asd empty");
                        db.formDao().insertAll(formNew);
                        System.out.println("asd mor ");
                    }else{
                        if(delete()) {
                            System.out.println("asd not empty");
                            db.formDao().insertAll(formNew);
                        }else{
                            throw new Exception();
                        }
                    }
                    return true;
                }catch (Exception e){
                    System.out.println("asd save errornya "+e.getMessage());
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                if(result){
                    SuperDialog.showDialogMessage(SuperForm.this,"Successfully","Survey anda berhasil tersimpan");
                }else{
                    SuperDialog.showDialogMessage(SuperForm.this,"Error","Terjadi kesalahan saat menyimpan data");
                }
            }
        }.execute();
    }

    private void getFilledData(final String to){
        final SuperDialog superDialog = new SuperDialog(this);
        superDialog.createProgressDialog("Getting your last session..",getLayoutInflater());
        superDialog.show();
        final JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("msisdn",new UserSession().getMsiSdn(this));
            jsonObject.put("id_form",to);
            jsonObject.put("trx_web",id_survey);
        }catch (Exception e){

        }
        System.out.println("asd param getFilled "+jsonObject.toString());
        final ApiService apiClient = RetrofitService.getRetrofit(Constant.getKey(24)).create(ApiService.class);
        final Call<String> call = apiClient.getFilledData(jsonObject.toString(),"{}");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    final JSONObject jsonResponse = new JSONObject(response.body());
                    System.out.println("asd jsonRespone "+jsonResponse.toString());
                    if(jsonResponse.getString("rc").equals("00")) {
                        final Intent intent = new Intent(SuperForm.this, SuperForm.class);
                        intent.putExtra(ARG_PARAM1, to);
                        intent.putExtra(ARG_PARAM2, id_survey);
                        intent.putExtra(ARG_PARAM4, true);
                        intent.putExtra(ARG_PARAM5, jsonResponse.getInt("id"));
                        intent.putExtra(ARG_PARAM3, jsonResponse.getJSONArray("data").toString());
                        superDialog.dismis();
                        startActivity(intent);
                        finish();
                    }else if(jsonResponse.getString("rc").equals("20")){
                        final Intent intent = new Intent(SuperForm.this, SuperForm.class);
                        intent.putExtra(ARG_PARAM1, to);
                        intent.putExtra(ARG_PARAM2, id_survey);
                        intent.putExtra(ARG_PARAM4, true);
                        intent.putExtra(ARG_PARAM5, "");
                        superDialog.dismis();
                        startActivity(intent);
                        finish();
                    }else{
                        superDialog.dismis();
                        SuperDialog.showDialogMessage(SuperForm.this,Constant.TITLE_SERVER_RESPONSE_FAILED,jsonResponse.getString("rm"));
                    }

                }catch (Exception e){
                    System.out.println("asd [SuperForm exception] "+e.getMessage());
                    superDialog.dismis();
                    SuperDialog.showDialogMessage(SuperForm.this,Constant.TITLE_SERVER_RESPONSE_FAILED,Constant.DESC_SERVER_RESPONSE_FAILED);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                superDialog.dismis();
                System.out.println("asd [SuperForm] filure "+t.getMessage());
                SuperDialog.showDialogMessage(SuperForm.this,"Failed","Oops, Something wrong please try again later");

            }
        });

    }

//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//        final String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }

//    public String getRealPathFromURI(Uri uri) {
//        String path = "";
//        if (getContentResolver() != null) {
//            final Cursor cursor = getContentResolver().query(uri, null, null, null, null);
//            if (cursor != null) {
//                cursor.moveToFirst();
//                final int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//                path = cursor.getString(idx);
//                cursor.close();
//            }
//        }
//        return path;
//    }

//    public void generateNoteOnSD(Context context, String sFileName, String sBody) {
//        try {
//            final File root = new File(Environment.getExternalStorageDirectory(), "Notes");
//            if (!root.exists()) {
//                root.mkdirs();
//            }
//            File gpxfile = new File(root, sFileName);
//            FileWriter writer = new FileWriter(gpxfile);
//            writer.append(sBody);
//            writer.flush();
//            writer.close();
//            Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
//        } catch (IOException e) {
//            System.out.println("gelo IOEXCeptio "+e.toString());
//        }
//    }

}



