package com.dak.las.sessions;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.List;

public class DeviceSession {

    Context context;
    public DeviceSession(Context context) {
        this.context = context;
    }

    public void setMainActivity(int value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession", Context.MODE_PRIVATE).edit();
        editor.putInt("mainActivity", value);
        editor.commit();

    }

    public Integer getMainActivity() {
        SharedPreferences prefs = context.getSharedPreferences("deviceSession",	Context.MODE_PRIVATE);
        int nma = prefs.getInt("mainActivity", 0);
        return nma;
    }

    public void setInitialized(Boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession", Context.MODE_PRIVATE).edit();
        editor.putBoolean("initialized", value);
        editor.commit();

    }

    public Boolean isInitialized() {
        SharedPreferences prefs = context.getSharedPreferences("deviceSession",	Context.MODE_PRIVATE);
        Boolean value = prefs.getBoolean("initialized", false);
        return value;
    }

    public void clearSession(){
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession",Context.MODE_PRIVATE).edit().clear();
        editor.commit();
    }

    public String getListMandatoryForm() {
        Gson gson = new Gson();
        SharedPreferences prefs = context.getSharedPreferences("deviceSession",	Context.MODE_PRIVATE);
        return prefs.getString("mandatory_form", "");
    }

    public void setListMandatoryForms(String json){
        SharedPreferences.Editor editor = context.getSharedPreferences("deviceSession",Context.MODE_PRIVATE).edit();
        editor.putString("mandatory_form",json);
        editor.commit();
    }

}
