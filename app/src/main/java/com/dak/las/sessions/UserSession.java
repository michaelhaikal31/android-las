package com.dak.las.sessions;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSession {

    public void setName(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession", Context.MODE_PRIVATE).edit();
        editor.putString("name", value);
        editor.commit();

    }

    public  String getName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("userSession",	Context.MODE_PRIVATE);
        String nma = prefs.getString("name", "");
        return nma;
    }

    public void setMsiSdn(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession", Context.MODE_PRIVATE).edit();
        editor.putString("msisdn", value);
        editor.commit();

    }

    public  String getMsiSdn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("userSession",	Context.MODE_PRIVATE);
        String nma = prefs.getString("msisdn", "");
        return nma;
    }

    public void setAccountType(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession", Context.MODE_PRIVATE).edit();
        editor.putString("account_type", value);
        editor.commit();

    }

    public  String getAccountType(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("userSession",	Context.MODE_PRIVATE);
        String nma = prefs.getString("account_type", "");
        return nma;
    }

    public void clearSession(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession",Context.MODE_PRIVATE).edit().clear();
        editor.commit();
    }



    public void setLoginState(Context context,Boolean value){
        SharedPreferences.Editor editor = context.getSharedPreferences("userSession", Context.MODE_PRIVATE).edit();
        editor.putBoolean("login_state", value);
        editor.commit();
    }

    public Boolean getLoginState(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("userSession",	Context.MODE_PRIVATE);
        return prefs.getBoolean("login_state",false);
    }

}
